const watch = require('grunt-contrib-watch');
const sass = require('node-sass');

module.exports = function(grunt) {
   require('load-grunt-tasks')(grunt);
   grunt.initConfig({
      sass_globbing: {
         my_target: {
            files: {
               'src/scss/main.scss': ['src/scss/**/*.scss'],
            },
         },
         options: {
            useSingleQuotes: false
         }
      },
      sass: {
         dist: {
            options: {
               implementation: sass,
               sourceMap: false,
            },
            files: [{
               expand: true,
               cwd: 'src/scss',
               src: ['main.scss'],
               dest: 'src/css',
               ext: '.css'
            }]
         }
      },
      autoprefixer: {
         options: {
            grid: true,
            browsers: ['last 4 versions', 'ie 10', 'ie 11']
         },
         dist: {
            files: {
               'src/css/main.css': 'src/css/main.css'
            }
         },
      },
      cssmin: {
         scss: {
            files: [{
               expand: true,
               cwd: 'src/css/',
               src: ['main.css'],
               dest: 'src/css/',
               ext: '.min.css'
            }]
         },
      },
      watch: {
         scripts: {
            files: ['src/scss/**/*.scss'],
            tasks: ['compile'],
         },
      },
   });

   grunt.registerTask('compile', [
      'sass_globbing',
      'sass:dist',
      'autoprefixer',
      'cssmin:scss'
   ]);
   grunt.registerTask('default', [
      'compile',
      'watch'
   ]);
}