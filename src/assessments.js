import wixUsers from 'wix-users';
import wixData from 'wix-data';

export async function healthAssessment() {
   const myData = await wixData.query('HealthAssessmentResponseDB')
      .contains('_owner', wixUsers.currentUser.id)
      .limit(1)
      .find()
      .then(res => {
         if (res.items.length == 0) {
            $w('#healthAssessmentBtn').show()
            $w('#healthAssessmentCheck').hide()
            $w('#healthAssessmentDetailsContainer').hide()
            return false
         } else {
            $w('#healthAssessmentBtn').hide()
            $w('#healthAssessmentCheck').show()
            $w('#healthAssessmentDetailsContainer').show()
            return res.items[0]
         }
      })

   if (myData != false) {
      $w('#myDataWeight').text = `${myData.weight.toString()} ${myData.weightUnit.toString()}`
      $w('#myDataHeight').text = `${myData.height.toString()} ${myData.heightUnit.toString()}`
      $w('#myDataWaist').text = `${myData.waist.toString()} ${myData.waistUnit.toString()}`
      $w('#myDataHips').text = `${myData.hips.toString()} ${myData.hipsUnit.toString()}`
      $w('#myDataHeartRate').text = `${myData.heartRateBpm.toString()} BPM`
      $w('#myDataBloodType').text = `${myData.bloodType.toString()}`
      $w('#myDataSleep').text = `${myData.sleepHours.toString()} Hours`
      $w('#myDataScreenTime').text = `${myData.screentime.toString()} Hours`

      if (myData.medication == 'No') {
         $w('#myDataMedicationTypes').text = 'None'
      } else {
         $w('#myDataMedicationTypes').text = `${myData.medicationTypes.toString().split(',').join('\n')}`
      }

      if (myData.preExistingConditions == 'No') {
         $w('#myDataHighRiskConditions').text = 'None'
      } else {
         $w('#myDataHighRiskConditions').text = `${myData.highRiskConditions.toString().split(',').join('\n')}`
      }

      if (myData.tobacco == 'No') {
         $w('#myDataTobaccoUnit').text = '0'
      } else {
         $w('#myDataTobaccoUnit').text = `${myData.tobaccoUnits.toString()}`
      }

      if (myData.alcohol == 'No') {
         $w('#myDataAlcoholUnit').text = '0'
      } else {
         $w('#myDataAlcoholUnit').text = `${myData.alcoholUnits.toString()}`
      }

      if (myData.recreationalDrugs == 'No') {
         $w('#myDataRecreationalDrugFrequency').text = 'Never'
         $w('#myDataRecreationalDrugTypes').text = 'None'
      } else {
         $w('#myDataRecreationalDrugFrequency').text = `${myData.recreationalDrugFrequency.toString()}`
         $w('#myDataRecreationalDrugTypes').text = `${myData.recreationalDrugTypes.toString().split(',').join('\n')}`
      }
   } else {
      //hide
   }

}

export async function fitnessAssessment() {
   const myData = await wixData.query('FitnessAssessmentResponseDB')
      .contains('_owner', wixUsers.currentUser.id)
      .limit(1)
      .find()
      .then(res => {
         if (res.items.length == 0) {
            $w('#fitnessAssessmentBtn').show()
            $w('#fitnessAssessmentCheck').hide()
            return false
         } else {
            $w('#fitnessAssessmentBtn').hide()
            $w('#fitnessAssessmentCheck').show()
            return res.items[0]
         }
      })
}

export async function dietAssessment() {
   const myData = await wixData.query('DietAssessmentResponseDB')
      .contains('_owner', wixUsers.currentUser.id)
      .limit(1)
      .find()
      .then(res => {
         if (res.items.length == 0) {
            $w('#dietAssessmentBtn').show()
            $w('#dietAssessmentCheck').hide()
            $w('#dietAssessmentDetailsContainer').hide()
            return false
         } else {
            $w('#dietAssessmentBtn').hide()
            $w('#dietAssessmentCheck').show()
            $w('#dietAssessmentDetailsContainer').show()
            return res.items[0]
         }
      })

   if (myData != false) {
      $w('#myDataDietProfile').text = `${myData.dietProfile.toString()}`
      
      $w('#myDataCalories').text = `${myData.calories.toString()} a day`
      $w('#myDataMealFrequency').text = `${myData.mealFrequency.toString()} a day`
      $w('#myDataBoughtMealFrequency').text = `${myData.boughtMealFrequency.toString()} a week`

      $w('#myDataCakesBiscuits').text = `${myData.cakesBiscuits.toString()}`
      $w('#myDataProcessedMeats').text = `${myData.processedMeats.toString()}`
      $w('#myDataSugaryDrinks').text = `${myData.sugaryDrinks.toString()}`
      $w('#myDataRefinedCarbs').text = `${myData.refinedCarbs.toString()}`
      $w('#myDataFriedFood').text = `${myData.friedFood.toString()}`
      $w('#myDataSweetsChocolate').text = `${myData.sweetsChocolate.toString()}`

      $w('#myDataBeansPulses').text = `${myData.beansPulses.toString()}`
      $w('#myDataFruitVeg').text = `${myData.fruitVeg.toString()}`
      $w('#myDataHealthyFats').text = `${myData.healthyFats.toString()}`
      $w('#myDataWholeGrains').text = `${myData.wholeGrains.toString()}`
      $w('#myDataLeanProtein').text = `${myData.leanProtein.toString()}`
      $w('#myDataNutsSeeds').text = `${myData.nutsSeeds.toString()}`

      if (myData.dietType.length == 0) {
         $w('#myDataDietType').text = 'None'
      } else {
         $w('#myDataDietType').text = `${myData.dietType.toString().split(',').join('\n')}`
      }

      if (myData.dietAllergies.length == 0) {
         $w('#myDataDietAllergies').text = 'None'
      } else {
         $w('#myDataDietAllergies').text = `${myData.dietAllergies.toString().split(',').join('\n')}`
      }

      if (myData.dietAversions.length == 0) {
         $w('#myDataDietAversions').text = 'None'
      } else {
         $w('#myDataDietAversions').text = `${myData.dietAversions.toString().split(',').join('\n')}`
      }
   } else {
      //hide
   }

}