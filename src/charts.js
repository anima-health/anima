import wixData from 'wix-data';
import wixUsers from 'wix-users';
import { getDate, convertDate, daysDiff, getTodayData, getMultiDayData, getDay, chartDataFillWeek } from "public/functions";

//TODO: WILL CHANGE INTO SPECFIC CHART BUILD
/**
 * function to creat a bar chart for daily data for a week
 * 
 * @param {Object} obj - Object for custom chart element
 * @param {String} obj.element - id of custome element
 * @param {String} obj.dataset - dataset to be used for the chart
 * @param {String} obj.type - type of the chart
 * @param {Object} obj.options
 */
export async function MonthChart(obj) {
   //creates chart custom element
   let chart = new chartJSAPI($w(`${obj.element}`));

   //query the dataset for the data
   const chartItems = await wixData
      .query(obj.dataset)
      .find()
      .then(res => res.items);

   const data = chartItems.map(item => item.data);
   const backgroundColor = chartItems.map(item => item.backgroundColor);
   const labels = chartItems.map(item => item.label);

   let dataset = []

   //creates array for the data options for the cahrt
   dataset = setDataset(data, labels, backgroundColor)

   //set the charts labels and data
   chart.data = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      datasets: dataset
   }

   //set chart tyoe
   obj.type ? chart.type = obj.type : null
}

/**
 * Gets data for the specific polar chart
 * 
 * @param {String} range - date range for chart data e.g. day, week, month
 * @returns dataset & label to be used for a polar chart custom element
 */
export async function getPolarChartData(range) {
   //query the dataset for the data
   const chartItems = await wixData
      .query('EveningHealthCheckResponseDB')
      .eq("uid", wixUsers.currentUser.id)
      .find()
      .then(res => res.items);

   //get today in correct date
   let today = getDate().toString()
   let todayObject = convertDate(today);

   //get data into correct format for chart
   const data = chartItems.map(item => {
      let dateObject = convertDate(item.date);
      let dayAgo = daysDiff(todayObject, dateObject)

      return {
         'daysAgo': dayAgo,
         'health': item.health,
         'energy': item.energy,
         'diet': item.diet,
         'fitness': item.fitness,
         'comfort': item.comfort,
         'mood': item.mood,
         'calmness': item.calmness,
         'social': item.social,
         'leisure': item.leisure,
         'work': item.work,
         'money': item.money,
         'life admin': item.lifeAdmin
      }
   });

   //get data either for a single day or multiple days
   let chartData
   if (range == 'day') {
      chartData = getTodayData(data)
   } else if (range == 'week') {
      chartData = getMultiDayData(data, 7)
   } else if (range == 'month') {
      chartData = getMultiDayData(data, 30)
   }

   //removes days ago key value pair
   let result = [];
   let labels = [];
   for (let i in chartData) {
      result.push(chartData[i]);
      labels.push(i);
   }
   result.shift()
   labels.shift()

   //polar chart color
   let colors = ['#FB1629', '#FD5808', '#FFE305', '#75FC01', '#00FF5B', '#00FFCA', '#06A2FF', '#0C4EFF', '#8A08FF', '#E503FF', '#F90A8F', '#FE1751']

   //set data into a dataset for the chart
   let datasets = [{
      data: result,
      backgroundColor: colors,
   }]

   //return data and labels for the custom polar chart
   return { datasets, labels }
}


//Main Charts

//Morning Health Check
export async function getMorningChartData(rolling = false) {
   const keys = ['mood', 'energy', 'health', 'comfort', 'sleep']
   const color = ['#FB1629', '#FFE305', '#75FC01', '#0C4EFF', '#8A08FF']

   let today = new Date();
   let nextweek = new Date(today.getFullYear(), today.getMonth(), today.getDate()-7);

   //get data from dataset
   let data = await wixData.query(`MorningHealthCheckResponseDB`)
      .limit(7)
      .find()
      .then(res => res.items);
   
   let arr = []
   for (var i = data.length - 1; i >= 0; i--) {
      let date = data[i].date;

      if( new Date(date) > new Date(nextweek) ){
         arr.push(data[i])
      }
   }

   let chartData = await getChartDataMain(arr, keys, color)

   //return data and labels for the bar chart
   return chartData
}

//Evening Health Check
export async function getEveningChartData() {
   const keys = ['health', 'mood', 'calmness', 'diet', 'lifeAdmin', 'energy', 'comfort', 'social', 'work', 'fitness', 'money', 'leisure']
   const color = ['#FB1629', '#FD5808', '#FFE305', '#75FC01', '#00FF5B', '#00FFCA', '#06A2FF', '#0C4EFF', '#8A08FF', '#E503FF', '#F90A8F', '#FE1751']

   //get data from dataset
   const data = await wixData
      .query('EveningHealthCheckResponseDB')
      .eq("uid", wixUsers.currentUser.id)
      .find()
      .then(res => res.items);
   
   let chartData = await getChartDataMain(data, keys, color)
   
   //return data and labels for the bar chart
   return chartData
}

//Daily Lifestyle Health Check
export async function getLifestyleChartData() {
   const keys = ['breakfastEnergy', 'breakfastMood', 'lunchEnergy', 'lunchMood', 'dinnerEnergy', 'dinnerMood', 'physicalActivityEnergy', 'physicalActivityMood', 'leisureActivityEnergy', 'leisureActivityMood']
   const color = ['#FB1629', '#FD5808', '#FFE305', '#00FF5B', '#0C4EFF', '#8A08FF']
   
   //get data from dataset
   const data = await wixData
   .query('LifestyleHealthCheckResponseDB')
   .eq("uid", wixUsers.currentUser.id)
   .find()
   .then(res => res.items);

   let chartData = await getChartDataMain(data, keys, color)
   
   //return data and labels for the bar chart
   return chartData
}

//Morning & Evening Health Check ( Mood, Health, Energy )
export async function getDailyData() {
   const datasets = ['EveningHealthCheckResponseDB', 'MorningHealthCheckResponseDB']
   const keys = ['health', 'mood', 'energy']
   const color = ['#FB1629', '#FD5808', '#FFE305']

   let dataArr = getArrObj(datasets)

   for (let i = 0; i < Object.keys(dataArr).length; i++) {
      const element = Object.keys(dataArr)[i];
      dataArr[`${element}`] = await wixData.query(element)
         .eq("uid", wixUsers.currentUser.id)
         .find()
         .then(res => res.items);
   }

   let chartData = await getCombinedChartDataMain(dataArr, keys, color)
   
   //return data and labels for the bar chart
   return chartData
}

//Morning & Evening Health Check Polar
export async function combinedPolar() {
   const dataset = ['EveningHealthCheckResponseDB', 'MorningHealthCheckResponseDB']
   const color = ['#FB1629', '#FD5808', '#FFE305', '#75FC01', '#00FF5B', '#00FFCA', '#06A2FF', '#0C4EFF', '#8A08FF', '#E503FF', '#F90A8F', '#FE1751']
   const keys = ['health', 'energy', 'diet', 'fitness', 'comfort', 'mood', 'calmness', 'social', 'leisure', 'work', 'money', 'lifeAdmin']

   //get data from mulitple dataset
   let dataArr = getArrObj(dataset)
   let today = getDate();
   for (let i = 0; i < Object.keys(dataArr).length; i++) {
      const element = Object.keys(dataArr)[i];
      dataArr[`${element}`] = await wixData.query(element)
         .eq("uid", wixUsers.currentUser.id)
         .eq("date", today)
         .find()
         .then(res => res.items);
   }

   //get the chart data from the arrays and set them to objects
   let array = dataArr[Object.keys(dataArr)[0]]
   let obj1 = getObjectFromArray(array, keys)

   array = dataArr[Object.keys(dataArr)[1]]
   let obj2 = getObjectFromArray(array, keys)

   //combine objects and average out value if needed
   let returnObj = {}
   for (let i = 0; i < keys.length; i++) {
      const key = keys[i]
      if (obj1[`${key}`] == undefined && obj2[`${key}`] == undefined) {
         returnObj[`${key}`] = 0
      } else if (obj1[`${key}`] == undefined) {
         returnObj[`${key}`] = obj2[`${key}`]
      } else if (obj2[`${key}`] == undefined) {
         returnObj[`${key}`] = obj1[`${key}`]
      } else {
         returnObj[`${key}`] = (obj1[`${key}`] + obj2[`${key}`]) / 2
      }
   }
   
   //setting the data and labels for the polar chart
   let labels = []
   let data = []

   for(let d in returnObj) {
      labels.push(d)
      data.push(returnObj[d])
   }

   let datasets = [
      {
         data: data,
         backgroundColor: color
      }
   ]

   return { datasets, labels }
}


//functions for data manipulation
export async function getChartDataMain(data, keys, color) {
   //create arrays for each chart data column
   let arrObj = getArrObj(keys)

   //map data for chart to date
   let newData = mapData(data, keys)

   //!rolling ? newData = chartDataFillWeek(newData) : null
   //For chart to always display Mon - Sun 
   newData = chartDataFillWeek(newData)
      
   //set the array of labels for the chart
   let dataLabels = formatLabels(newData)
      
   //rearrange data into the correct format for the dataset
   let chartData = formatData(newData, keys, arrObj)
   let labels = chartData.labels
      
   //set up datasets for the chart
   let dataset = setDataset(chartData.arrObj, dataLabels, color)

   return { dataset, labels }
}

/**
 * function to pull out data from an object in an array
 * @param {Array} array - array of data from a dataset
 * @param {Array} keys - keys for which data to retrieve
 * @returns object of data to be used in chart
 */
export function getObjectFromArray(array, keys) {
   let final = {}

   for (let i = 0; i < array.length; i++) {
      const el = array[i];
      for(let key in el){
         for (let k = 0; k < keys.length; k++) {
            const element = keys[k];
            if (element == key) {
               final[key] = el[`${key}`]
            }
         }
      }
   }

   return final
}

/**
 * Function to create an object of arrays
 * 
 * @param {Array} keys - Array of labels to be converted into named arrays
 * @returns Object of arrays
 */
export function getArrObj(keys) {
   let arrObj = {}

   //create blank arrays for all keys
   for (let i = 0; i < keys.length; i++) {
      const element = keys[i];
      arrObj[element] = []
   }

   return arrObj
}

/**
 * Function to set all the chart data into the correct format for the chart
 * 
 * @param {Array} data - Array of chart data
 * @param {Array} keys - Array of labels to used as keys for arrays
 * @param {Object} arrObj - Object of arrays
 * @returns Array of labels for the chart & Object of arrays containing each set of data for the chart
 */
export function formatData(data, keys, arrObj) {
   let labels = []

   for (let i = 0; i < data.length; i++) {
      const element = data[i];
      labels.push(element.date)

      for (let j = 0; j < keys.length; j++) {
         const element = keys[j];
         arrObj[`${element}`].push(data[i][element])
      }
   }

   return {labels, arrObj}
}

/**
 * Funciton to return an array of lables
 * 
 * @param {Array} data - Array of chart data
 * @returns Array of labels for the data in the chart
 */
export function formatLabels(data) {
   let labels = []
   for (let key in data[0]) {
      key = key.split(/(?=[A-Z])/).join(" ")
      key = key.charAt(0).toUpperCase() + key.slice(1);
      labels.push(key)
   }
   labels.shift()

   return labels
}

/**
 * Function to create chart.js datasets from data object
 * 
 * @param {Object} data - Object of arrays which contain all the chart data
 * @param {Array} labels - Array of labels for the chart
 * @param {Array} colors - Array of colors for each chart dataset
 * @returns Array of datasets to be used by chart.js
 */
export function setDataset(data, labels, colors) {
   let dataset = []

   Object.values(data).forEach((value, idx) => {
      dataset[idx] = {
         fill: 'false',
         data: value,
         label: labels[idx],
         'backgroundColor': colors[idx],
         'borderColor': colors[idx],
         lineTension: 0,
      }
   });
   return dataset
}

/**
 * 
 * @param {Array} data - Data from datset
 * @param {Array} keys - Array of which fields to use from dataset data
 * @returns Array of data seperated by date
 */
export function mapData(data, keys) {
   data.reverse()

   let newData = data.map(item => {
      let day = new Date(convertDate(item['date'])).getDay()
      let obj = {
         date: getDay(day),
      }
      for (let i = 0; i < keys.length; i++) {
         const element = keys[i];
         obj[element] = item[`${element}`]
      }
      
      return obj
   });

   return newData
}

export async function getCombinedChartDataMain(dataArr, keys, color) {
   //create arrays for each chart data column
   let arrObj = getArrObj(keys)

   //fill out data arrays for the whole week
   for (let i = 0; i < Object.keys(dataArr).length; i++) {
      const key = Object.keys(dataArr)[i]
      let data = dataArr[key]
      let newData = mapData(data, keys)
      newData = chartDataFillWeek(newData)
      dataArr[key] = newData
   }

   let final = []
   let array = dataArr[Object.keys(dataArr)[0]]

   //combine array to get array of total values
   for (let i = 0; i < array.length; i++) {
      const el = array[i];

      for (let j = 1; j < Object.keys(dataArr).length; j++) {
         const element = dataArr[Object.keys(dataArr)[j]];

         for (let k = 0; k < element.length; k++) {
            if (element[k].date == el.date){
               final.unshift(sumObjectsByKey(element[k], el))
            }
         }
      }
   }

   //average out values in array
   for (let i = 0; i < final.length; i++) {
      const element = final[i];
      let newVal

      for (let j = 0; j < Object.keys(element).length; j++) {
         const el = Object.values(element)[j];
         if (typeof el == 'number') {
            newVal = el / Object.keys(dataArr).length
         } else {
            newVal = el
         }
         final[i][Object.keys(element)[j]] = newVal
      }
   }

   //map data for chart to date
   let newData = mapData(final, keys)

   //!rolling ? newData = chartDataFillWeek(newData) : null
   //For chart to always display Mon - Sun 
   newData = chartDataFillWeek(newData)
      
   //set the array of labels for the chart
   let dataLabels = formatLabels(newData)
      
   //rearrange data into the correct format for the dataset
   let chartData = formatData(final, keys, arrObj)

   let labels = chartData.labels
      
   //set up datasets for the chart
   let dataset = setDataset(chartData.arrObj, dataLabels, color)

   return { dataset, labels }
}

/**
 * 
 * @param  {...Object} objs - 
 * @returns 
 */
export function sumObjectsByKey(...objs) {
   let returnObj = {}
   for (let i = 0; i < Object.keys(objs[0]).length; i++) {
      let finalVal
      const element = objs[0];
      let startVal = Object.values(element)[i]

      finalVal = startVal

      for (let j = 1; j < objs.length; j++) {
         const el = objs[j];
         let otherVal = Object.values(el)[i]

         if(typeof startVal == 'string') {
            finalVal = startVal
         } else if(startVal == null && otherVal == null) {
            finalVal = null
         } else {
            finalVal += otherVal
         }
      }
      returnObj[`${Object.keys(objs[0])[i]}`] = finalVal
   }
   return returnObj
}