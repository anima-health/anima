const sliderProps = {
	fill: "#05FFFF",
	background: "#205685",
};

function applyFill(slider, sliderValue) {
	const percentage = (100 * (slider.value - slider.min)) / (slider.max - slider.min);
	const bg = `linear-gradient(90deg, ${sliderProps.fill} ${percentage}%, ${sliderProps.background} ${percentage + 0.1}%)`;
	slider.style.background = bg;
	sliderValue.setAttribute("data-length", slider.value);
}

function moveTooltip(slider, sliderValue) {
    let tooltip = slider.querySelector("#tooltip");
    tooltip.innerHTML = sliderValue.dataset.length

    let per = (sliderValue.dataset.length / slider.dataset.max) * 100;

    tooltip.style.left = per + '%'
}

const all = document.querySelectorAll(".input__slider--container");

for (let i = 0; i < all.length; i++) {
    const slider = all[i];
    const sliderValue = slider.querySelector(".length__title");

    slider.querySelector("input").addEventListener("input", event => {
        sliderValue.setAttribute("data-length", event.target.value);
        applyFill(event.target, sliderValue);
        
        //moveTooltip(slider, sliderValue);
    });
console.log(slider)
console.log(sliderValue)
    applyFill(slider.querySelector("input"), sliderValue);
    //moveTooltip(slider, sliderValue);
    
    const notch = parseInt(slider.querySelector("input").max) + 1

    for (let i = 0; i < notch; i++) {
        const el = document.createElement('span');
        const width = i * (100 / (notch - 1)) + '%';

        el.style.left = width;

        slider.querySelector('.input__slider--notch').appendChild(el)
    }
}