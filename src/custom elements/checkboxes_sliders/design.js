/**
 * HTML template for the custom element container
 * 
 * @param {Array} checkbox - array of HTML templates for the checkboxes
 * @param {Array} slider - array of HTML templates for the sliders
 * @param {String} name - ID for the container
 */
 export function renderChecksliderWrapper(checkbox, slider, name) {
   //capitalise the first letter fo the name
   let displayName = name.charAt(0).toUpperCase() + name.slice(1)
   return `
      <div id="issueDetailsConatiner">
         <div id="${name}Container">
            <div id="${name}CheckboxConatiner">
               ${checkbox.join('')}
            </div>
            <div id="${name}SliderContainer" class='slider__container'>
               <h2 class="font_2">${displayName} Severity</h2>
               <p class='hint'>Select the severity level for each ${name}:</p>
               
               ${slider.join('')}
            </div>
         </div>
      </div>`;
}
//<span id="sliderText">No ${name}s selected</span>
/**
 * Checkbox HTML template
 * 
 * @param {String} name - name of group the checkboxes belong to
 * @param {Object} item - checkbox data object 
 * @param {String} item.name - value of the checkbox
 */
export function renderChecksliderCheck(item, name) {
   // remove any spaces & foward slashs from item.name
   let id = item.name.replace(/\s/g, '');
   id = id.replace(/\//g, '');
   return `
      <label class="${name}Checkbox">
         <input class="input__checkbox" type="checkbox" name="${item.name}" value="${item.name}" id="${id}Check">
         <span class="input__checkbox--text">${item.name}</span>
      </label>`;
}

/**
 * Slider HTML template
 * 
 * @param {String} name - name of group the sliders belong to
 * @param {Object} item - slider data object 
 * @param {String} item.name - value of the slider
 */
export function renderChecksliderSlider(item, name) {
   // remove any spaces & foward slashs from item.name
   let id = item.name.replace(/\s/g, '');
   id = id.replace(/\//g, '');
   return `
      <div id="${id}Slider" class="length range__slider ${name}Slider" data-min="0" data-max="10">
         <div class="length__title field-title" data-length='0'></div>
         <div class="tooltipContainer">
            <div id="tooltip"></div>
         </div>
         <span class="slider_name">${item.name}</span>
         <input id="${id}" class="slider" type="range" step='1' min="0" max="10" value="0" />
         <span class="notch"></span>
      </div>`;
}

//CSS for the custom element
export const ChecksliderCSS = `
   #symptomCheckboxConatiner, #symptomSliderContainer, #painSliderContainer, #painCheckboxConatiner, #painTypeCheckboxConatiner {
      display: flex;
      flex-wrap: wrap;
      position: relative;
   }

   .symptomCheckbox, .painCheckbox {
      margin-bottom: 17px;
      width: 50%;
   }

   .symptomSlider, .painSlider{
      width: 100%;
      display: flex;
      flex-direction: column;
      margin: 20px 80px;
      text-align: center;
   }

   .symptomSlider, .painSlider, .hidden{
      visibility: hidden;
      height: 0;
   }

   .hiddenText{
      visibility: hidden;
      height: 0;
      display: none;
   }

   .range__slider.active{
      visibility: visible;
      height: 125px;
      margin: 25px 0;
   }

   #sliderText{
      height: 124px;
      width: 100%;
      text-align: center;
      background: #80EFFF;
      font-size: 36px;
      line-height: 124px;
      margin: 0;
      color: #205685;
      font-family: din-next-w01-light, din-next-w02-light, din-next-w10-light, sans-serif;
      font-weight: 100;
   }

   .input__checkbox--text {
      position: relative;
      padding-left: 35px;
      cursor: pointer;
      display: inline-block;
      line-height: 19px;
      user-select: none; 
      font-size: 19px;
      font-family: avenir-lt-w01_35-light1475496, avenir-lt-w05_35-light, sans-serif;
      color: #2B5672
   }

   label{
      width: 50%;
      margin-bottom: 10px;
      text-align: left;
   }

   .input__checkbox--text:after {
      content: "";
      position: absolute;
      display: none; 
   }

   .input__checkbox:checked + .input__checkbox--text:after {
      display: block; 
   }

   .input__checkbox:checked + .input__checkbox--text:before {
      background-color: #05ffff69; 
   }

   .input__checkbox:disabled + .input__checkbox--text {
      cursor: initial; 
   }

   .input__checkbox:disabled + .input__checkbox--text:before {
      background-color: #ebebe4;
      border: 2px solid #ebebe4; 
   }

   .input__checkbox--text:hover:before {
      background-color: #05ffff69; 
   }

   .input__checkbox--text:after {
      left: 7px;
      top: 18%;
      width: 3px;
      height: 8px;
      border: solid #67E891;
      border-width: 0 2px 2px 0;
      transform: rotate(46deg);
   }

   .input__checkbox--text:before {
      content: '';
      left: 0;
      top: 2%;
      position: absolute;
      height: 14px;
      width: 14px;
      border: 2px solid #80EFFF;
      transition: all .2s;
      border-radius: 5px;
   }

   .input__checkbox {
      position: absolute;
      opacity: 0;
      pointer-events: none;
      width: auto; 
   }

   .range__slider {
      position: relative;
      width: 100%;
      height: 0;
      display: flex;
      justify-content: center;
      align-items: center;
      border-radius: 8px;
      margin: 0;
   }

   .slider {
      -webkit-appearance: none;
      width: calc(100% - (70px));
      height: 7px;
      border-radius: 5px;
      background: rgba(255, 255, 255, 0.314);
      outline: none;
      padding: 0;
      margin: 0;
      cursor: pointer;
      background: linear-gradient(90deg, rgb(4, 230, 136) 10%, rgb(32, 86, 133) 10.1%);
   }
   
   .slider::-webkit-slider-thumb {
      -webkit-appearance: none;
      width: 25px;
      height: 25px;
      border-radius: 50%;
      background: #1DABBF;
      cursor: pointer;
      transition: all 0.15s ease-in-out;
   }

   .slider::-webkit-slider-thumb:hover {
      background: #04E688;
      transform: scale(1.2);
   }

   .slider::-moz-range-thumb {
      width: 20px;
      height: 20px;
      border: 0;
      border-radius: 50%;
      background: #67E891;
      cursor: pointer;
      transition: background 0.15s ease-in-out;
   }

   .slider::-moz-range-thumb:hover {
      background: #59b678;

      .slider {
         background: linear-gradient(90deg, rgb(4, 230, 136) 10%, rgb(29 171 191) 10.1%);
      }
   }

   .slider_name {
      display: block;
      padding-bottom: 71px;
      color: #1BACBF;
      width: calc(100% - 70px);
      text-align: left;
      font-size: 26px;
      font-family: var(--font_5);
      font-weight: 100;
   }

   .notch {
      width: calc(100% - (93px));
      height: 10px;
      position: absolute;
      top: 110%;
   }

   .notch span {
      display: inline-block;
      width: 2px;
      height: 10px;
      position: absolute;
      background: #205685;
      top: 0;
   }

   .tooltipContainer{
      width: calc(100% - (93px));
      position: absolute;
      top: 0;
   }

   #tooltip{
      position: absolute;
      top: 54px;
      width: 20px;
      transform: translateX(-50%);
      background: #04a6ff;
      color: white;
      padding: 6px 6px;
      border-radius: 6px;
      font-size: 14px;
   }

   #tooltip:after{
      content: '';
      position: absolute;
      border: 8px solid transparent;
      top: 100%;
      left: calc(50% - 8px);
      border-top-color: #04a6ff;
   }

   h2{
      font-size: 29px;
      font-family: 'Proxima Nova';
      color: #205784;
      font-weight: 900;
   }

   .hint{
      width: 100%;
      text-align: center;
      margin: 0;

      color: #205685;
      font-size: 20px;
      font-weight: 100;
      font-family: var(--font_8);
   }

   .slider__container{
      visibility: hidden;
   }

   .slider__container.active{
      visibility: visible;
   }

   .font_2{
      color: #205685;
      font-size: 28px;
      font-family: var(--font_2);
      font-weight: 400;
   }
`;