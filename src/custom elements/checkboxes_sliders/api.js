//API
export class CheckSliderAPI {
   /**
    * Constructor for the custom element
    * 
    * @param {String} el - element ID that the custom element with be attached to
    * @param {String} globalTemp - HTML template string for the custom element container
    * @param {String} checkboxTemp - HTML template for the checkboxes
    * @param {String} sliderTemp - HTML template for the sliders that will appear after the corrosponding checkbox is selected
    * @param {String} name - identifier for the custom element
    */
   constructor(el, globalTemp, checkboxTemp, sliderTemp, name) {
      this.el = el
      this._globalTemp = globalTemp
      this._checkboxTemp = checkboxTemp
      this._sliderTemp = sliderTemp
      this._name = name
   }

   //getter for the css attribute 
   get css() {
      return this._css;
   }

   /**
    * setter for the css attribute
    * 
    * @param {String} css - CSS string for the custom elenebt
    */
   set css(css) {
      this._css = css;
      this.el.setAttribute('css', css);
   }

   //getter for the data attribute
   get data() {
      return this._data;
   }

   /**
    * setter for the data attribute
    * 
    * @param {Array} d - array of data to be used for the custom element
    */
   set data(d) {
      this._data = d;
      this.render();
   }

   //Render the custom element by setting the HTML templates to the HTML attribute
   render() {
      let checkTemp = this._data.map(_ => this._checkboxTemp(_, this._name));
      let sliderTemp = this._data.map(_ => this._sliderTemp(_, this._name));
      let html = this._globalTemp(checkTemp, sliderTemp, this._name);
      this.el.setAttribute('html', html);
      this.el.setAttribute('name', this._name);
   }
}