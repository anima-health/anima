class CheckSlider extends HTMLElement {

   //constructor for the custom element
   constructor() {
      super();
      this._shadow = this.attachShadow({
         mode: 'open'
      });
      this._styleElem = document.createElement('style');
      this._styleElem.textContent = ``;
      this._root = document.createElement('div');
      this._shadow.appendChild(this._styleElem);
      this._shadow.appendChild(this._root);
      //color properties for the slider
      this._sliderProps = {
         fill: "#04E688",
         background: "#205685",
      };
   }

   /**
    * function to style the slider the correct color
    * 
    * @param {Element} slider - range input element for the current slider
    * @param {Element} sliderValue - element that holds the data-length attribute for the current slider
    */
   applyFill(slider, sliderValue) { 
      const percentage = (100 * (slider.value - slider.min)) / (slider.max - slider.min);
      const bg = `linear-gradient(90deg, ${this._sliderProps.fill} ${percentage}%, ${this._sliderProps.background} ${percentage + 0.1}%)`;
      slider.style.background = bg;
      sliderValue.setAttribute("data-length", slider.value);
   }

   /**
    * function to keep the tooltip of the slider on the correct position
    * 
    * @param {Element} slider - range input element for the current slider
    * @param {Element} sliderValue - element that holds the data-length attribute for the current slider
    */
   moveTooltip(slider, sliderValue) {
      let tooltip = slider.querySelector("#tooltip");
      tooltip.innerHTML = sliderValue.dataset.length
      tooltip.style.left = sliderValue.dataset.length + '0%'
   }

   //attributes that are watched for changes
   static get observedAttributes() {
      return ['html', 'css'];
   }

   /**
    * Sets the new value for the watched attributes
    * 
    * @param {String} name - name of the attribute to be changes
    * @param {String} newValue - new value for the attribute
    * @param {String} oldValue - old value for the attribute
    */
   attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'html')
         this.html = newValue;
      if (name === 'css')
         this.css = newValue;
      if (name === 'name')
         this.name = newValue;
   }

   //getter for the html attribute
   get html() {
      return this._html;
   }

   /**
    * setter for the hmtl attribute
    * 
    * @param {String} t - HTML template string
    */
   set html(t) {
      this._html = t;
      sessionStorage.setItem('html', t);
      if (this._connected)
         this.render();
   }

   //getter for the css attribute
   get css() {
      return this._css;
   }

   /**
    * setter for the css attribute
    * 
    * @param {String} s - css string for the custom element
    */
   set css(s) {
      this._css = s;
      sessionStorage.setItem('css', s);
      this._styleElem.textContent = s;
   }

   //getter for the name attribute
   get name() {
      return this._name;
   }

   /**
    * setter for the name attribute
    * 
    * @param {String} s - string for the name of the custom element
    */
   set name(s) {
      this._name = s;
      sessionStorage.setItem('name', s);
   }

   /**
    * function to hide / show sliders based on checked checkboxes
    * 
    * @param {Event} e - event handler
    * @param {Element} el - current custom element
    */
   showSlider(e, el) {
      // replace all spaces and foward slashes for the element id
      let id = e.path[0].value.replace(/\s/g, '');
      id = id.replace(/\//g, '');

      let slider = el._root.querySelector(`#${id}Slider`)

      //checks if clikced checkbox is currently active
      if (el.checkedBoxes.includes(e.target.value)) {
         //remove clicked checkbox from active checkboxes
         let index = el.checkedBoxes.indexOf(e.target.value);
         if (index !== -1) {
            el.checkedBoxes.splice(index, 1);
         }
         slider.classList.remove('active')
         el._root.querySelector(`.hint`).classList.remove('active')

         //remove associated slider from active sliders
         for (let i = 0; i < el.sliderValues.length; i++) {
            const element = el.sliderValues[i];
            if (element.checkbox == e.target.value) {
               index = el.sliderValues.indexOf(element)
               if (index !== -1) {
                  el.sliderValues.splice(index, 1);
               }
            }
         }
      } else {
         //adds clicked checkbox to active list and associated slider to actove slider lost
         el.checkedBoxes.push(e.target.value)
         slider.classList.add('active')
         el._root.querySelector(`.slider__container`).classList.add('active')

         //add default slider data to array
         let data = {
            checkbox: e.target.value.replace(/\s/g, '').replace(/\//g, ''),
            value: '0'
         }
         el.sliderValues.push(data)
      }

      //emit data back out to user
      this.dispatchData(el)
   }

   /**
    * function to set slider values to global data to be emitted back to the user
    * 
    * @param {Event} e - event handler
    * @param {Element} el - current custom element
    * @param {Element} slider - range input element for the current slider
    * @param {Element} sliderValue - element that holds the data-length attribute for the current slider
    */
   setSlider(e, el, sliderValue, slider) {

      sliderValue.setAttribute("data-length", e.target.value);
      this.applyFill(e.target, sliderValue);
      this.moveTooltip(slider, sliderValue);

      //checks to see if any sliders are actove
      if (el.sliderValues.length == 0) {
         //adds new slider data to active slider data
         let data = {
            checkbox: e.target.id,
            value: e.target.value
         }
         el.sliderValues.push(data)
      } else {
         let exisit = false
         //checks to see if current slider already exisits in active slider list
         for (let i = 0; i < el.sliderValues.length; i++) {
            const element = el.sliderValues[i];
            //update slider if found in active slider lost
            if (element.checkbox == e.target.id) {
               element.value = e.target.value
               exisit = true
            }
         }
         //if current slider is not in active slider list add it 
         if (exisit == false) {
            let data = {
               checkbox: e.target.id,
               value: e.target.value
            }
            el.sliderValues.push(data)
         }
      }

      //emit slider data to user
      this.dispatchData(el)
   }

   /**
    * Dispatch custom event with attached object of checked checkboxes and corrospding slider values
    * 
    * @param {Element} el - the current custom element
    */
   dispatchData(el) {
      //Set object of checked checkboxes and slider values to be dispatched
      let details = {
         checkboxes: el.checkedBoxes,
         sliders: el.sliderValues
      }

      //Dispatch event when called from event listener on wix page
      this.dispatchEvent(new CustomEvent('my-event', {
         detail: details
      }));
   }

   //Set the css and html attributes and render element
   connectedCallback() {
      let savedHtml = sessionStorage.getItem('html');
      let savedCss = sessionStorage.getItem('css');
      if (savedCss && savedCss !== 'undefined' && !this._css)
         this.css = savedCss;
      if (savedHtml && savedHtml !== 'undefined' && !this._html)
         this._html = savedHtml;
      this._connected = true;
      if (this._html) {
         this.render();
      }
   }

   //Render function for the custom element
   render() {
      this._root.innerHTML = this._html;
      this.checkedBoxes = [];
      this.sliderValues = [];
      this.sliders = [];
      let checkboxes = this._root.querySelectorAll('.input__checkbox');
      let el = this;

      //add click event listener to all the checkboxes on the custom element
      for (var i = 0; i < checkboxes.length; i++) {
         checkboxes[i].addEventListener('click', e => this.showSlider(e, el), false)
      }

      let sliders = this._root.querySelectorAll('.range__slider');

      for (let i = 0; i < sliders.length; i++) {
         const slider = sliders[i];
         const sliderValue = slider.querySelector(".length__title");

         //adds input event listener to all the sliders in the custom element
         slider.querySelector('input').addEventListener('input', e => this.setSlider(e, el, sliderValue, slider), false)

         let id = slider.querySelector('input').id;
         el.sliders[id] = '0';

         //adds the slider fill style
         this.applyFill(slider.querySelector("input"), sliderValue);
         //makes sure the tooltip of the slider is in the correct position
         this.moveTooltip(slider, sliderValue);

         // adds in the visual notches to the custom element
         const notch = parseInt(slider.querySelector("input").max) + 1
         for (let i = 0; i < notch; i++) {
            const el = document.createElement('span');
            const width = i * (100 / (notch - 1)) + '%';

            //sets the correct position for every notch
            el.style.left = width;

            slider.querySelector('.notch').appendChild(el)
         }
      }
   }
}

//Define the checkbox with slider element for use on wix pages, used as the element source
customElements.define('check-slide', CheckSlider);