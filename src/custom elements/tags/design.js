/**
 * Tag HTML template
 * 
 * @param {Object} item - tag data object 
 * @param {String} item.name - value of the tag 
 */
export function renderTag(item) {
   return `
      <label for="${item.name}Tag">
         <input class='tag' type="checkbox" value='${item.name}' name="${item.name}" id="${item.name}Tag">
         <span class="tag_text">${item.name}</span>
      </label>`
}

/**
 * HTML template for the container for all the tags
 * 
 * @param {Array} tag - Array of HTML templates of tag elements to be displayed in container 
 * @param {String} name - unique name of the container
 */
export function renderTagWrapper(tag, name) {
   return `
      <div>
         <div id="${name}Container">
            <div class='tagContainer' id="${name}TagConatiner">
               ${tag.join('')}
            </div>
         </div>
      </div>`
}

//CSS for the tag conatiner and the tag element
export const TagCSS = `
   .tag{
      position: absolute;
      pointer-events: none;
      width: auto;
      transform: translate(100%, 100%);
      z-index: -1;
      display: contents;
   }

   .tag:checked + .tag_text {
      background-color: #04E688;
      border-color: #5EE08A;
   }

   .tag_text{
      display: inline-block;
      margin: 5px;
      background: #205685;
      border: 1px solid #205685;
      color: white;
      padding: 10px 15px;
      border-radius: 10px;
      transition: all .2s;
      font-size: 14px;
      font-family: avenir-lt-w01_35-light1475496, avenir-lt-w05_35-light, sans-serif;
      font-weight: 400;
   }

   .tag_text:hover{
      background-color: #A4F3C5;
      border-color: #C9FCF2;
      color: #205685;
   }

   .tagContainer{
      text-align: center
   }
`