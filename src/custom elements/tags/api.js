export class TagAPI {
   /**
    * Constructor for the tag custom element
    * 
    * @param {Element} el - element ID that the custom element with be inserted into
    * @param {String} globalTemp - HTML template for the custom element container
    * @param {String} tagTemp - HTML template for the actual tag element
    * @param {String} name - identifier sent to the custom element
   */
   constructor(el, globalTemp, tagTemp, name) {
      this.el = el
      this._globalTemp = globalTemp
      this._tagTemp = tagTemp
      this._name = name
   }

   //getter for the css attribute for the custom element
   get css() {
      return this._css
   }

   /**
    * setter for the css attribute for the custom element
    * 
    * @param {String} css - string containing css for the custom element
    */
   set css(css) {
      this._css = css
      this.el.setAttribute('css', css)
   }

   //getter for the data attribute for the custom element
   get data() {
      return this._data
   }

   /**
    * setter for the data attribute for the custom element
    * 
    * @param {Array} data - array of the data for the custom element
    */
   set data(data) {
      this._data = data
      this.render()
   }

   //render function for the custom element, sets the HTML templates as the HTML attribute for the element
   render() {
      let template = this._data.map(_ => this._tagTemp(_, this._name))
      let html = this._globalTemp(template, this._name)
      this.el.setAttribute('html', html)
   }
}