class Tag extends HTMLElement {

   //Constructor for the custom element
   constructor() {
      super();
      this._shadow = this.attachShadow({
         mode: 'open'
      });
      this._styleElem = document.createElement('style');
      this._styleElem.textContent = '';
      this._root = document.createElement('div');
      this._shadow.appendChild(this._styleElem);
      this._shadow.appendChild(this._root);
   }

   //Attributes that are watched for changes
   static get observedAttributes() {
      return ['html', 'css'];
   }

   /**
    * Sets the new value for the watched attributes
    * 
    * @param {String} name - name of the attribute to be changes
    * @param {String} oldValue - old value for the attribute
    * @param {String} newValue - new value for the attribute
    */
   attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'html')
         this.html = newValue;
      if (name === 'css')
         this.css = newValue;
   }

   //Getter for the html attribute for the custom element
   get html() {
      return this._html;
   }

   /**
    * Setter for the html attribute for the custom element, re-renders the element after change
    * 
    * @param {String} html - HTML template string
    */
   set html(html) {
      this._html = html;
      sessionStorage.setItem('html', html);
      if (this._connected)
         this.render();
   }

   //Getter for the css attribute for the custom element
   get css() {
      return this._css;
   }

   /**
    * Setter for the css attribute for the custom element
    * 
    * @param {String} css - string containing css for the custom element
    */
   set css(css) {
      this._css = css;
      sessionStorage.setItem('css', css);
      this._styleElem.textContent = css;
   }

   /**
    * Function for when the tag is clicked
    * 
    * @param {Event} e - event object
    * @param {Element} el - the current custom element
    */
   tagClick(e, el) {
      //If the tag is checked add to the array of checked tags
      if (e.target.checked == true) {
         if (!el.tags.includes(e.target.value)) {
            el.tags.push(e.target.value)
         }
      //If the tag is not checked remove it from the array of checked tags
      } else {
         let index = el.tags.indexOf(e.target.value)
         if (index != -1) {
            el.tags.splice(index, 1)
         }
      }
      //send array of checked tags to be dispathced in custom event
      el.dispatchData(el)
   }

   /**
    * Dispatch custom event with attached object of checked tags
    * 
    * @param {Element} el - the current custom element
    */
   dispatchData(el) {
      //Set object of checked tags to be dispatched
      let details = {
         tags: el.tags
      }

      //Dispatch event when called from event listener on wix page
      this.dispatchEvent(new CustomEvent('my-event', {
         detail: details
      }))
   }

   //Set the css and html attributes and render element
   connectedCallback() {
      let savedHtml = sessionStorage.getItem('html');
      let savedCss = sessionStorage.getItem('css');
      if (savedCss && savedCss !== 'undefined' && !this._css)
         this.css = savedCss;
      if (savedHtml && savedHtml !== 'undefined' && !this._html)
         this.html = savedHtml;
      this._connected = true;
      if (this._html)
         this.render();
   }

   //Render function for the custom element
   render() {
      this._root.innerHTML = this._html;
      this.tags = [];
      let el = this;
      let tags = this._root.querySelectorAll('.tag')
      
      //Add click event listener to all tag custom elements and run function when clicked
      for (let i = 0; i < tags.length; i++) {
         tags[i].addEventListener('click', e => this.tagClick(e, el), false)
      }
   }
}

//Define the tag element for use on wix pages, used as the element source
customElements.define('tag-ele', Tag)