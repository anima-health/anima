//API
export class CheckboxAPI {
   /**
    * onstructor for the checkbox custom element
    * 
    * @param {String} el - element ID that the custom element with be inserted into
    * @param {String} globalTemp - HTML template for the custom element container
    * @param {String} checkboxTemp - HTML template for the actual checkbox element
    * @param {String} name - identifier sent to the custom element
    * @param {Boolean} col - boolean to set the element in one column or two
    */
   constructor(el, globalTemp, checkboxTemp, name, col = false) {
      this.el = el
      this._globalTemp = globalTemp
      this._checkboxTemp = checkboxTemp
      this._name = name
      this._col = col
   }

   //getter for the css attribute for the custom element
   get css() {
      return this._css;
   }

   /**
    * setter for the css attribute for the custom element
    * 
    * @param {String} css - string containing css for the custom element
    */
   set css(css) {
      this._css = css;
      this.el.setAttribute('css', css);
   }

   //getter for the checked attribute for the custom element
   get checked() {
      return this._checked;
   }

   /**
    * setter for the checked attribute for the custom element
    * 
    * @param {Array} c - array of checked checkboxes 
    */
   set checked(c) {
      this._checked = c;
      this.render();
   }

   //getter for the data attribute for the custom element
   get data() {
      return this._data;
   }

   /**
    * setter for the data attribute for the custom element
    * 
    * @param {Array} d - array of the data for the custom element
    */
   set data(d) {
      this._data = d;
      this.render();
   }

   //render function for the custom element, sets the HTML templates as the HTML attribute for the element
   render() {
      let checkTemp = this._data.map(_ => this._checkboxTemp(_, this._name, this._col));
      let html = this._globalTemp(checkTemp, this._name);
      this.el.setAttribute('html', html);
      this.el.setAttribute('name', this._name);
      this.el.setAttribute('checked', this._checked);
   }
}