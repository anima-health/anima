class Check extends HTMLElement {

   //Constructor for the custom element
   constructor() {
      super();
      this._shadow = this.attachShadow({
         mode: 'open'
      });
      this._styleElem = document.createElement('style');
      this._styleElem.textContent = ``;
      this._root = document.createElement('div');
      this._shadow.appendChild(this._styleElem);
      this._shadow.appendChild(this._root);
   }

   //Attributes that are watched for changes
   static get observedAttributes() {
      return ['html', 'css', 'checked'];
   }

   /**
    * Sets the new value for the watched attributes
    * 
    * @param {String} name - name of the attribute to be changes
    * @param {String} oldValue - old value for the attribute
    * @param {String} newValue - new value for the attribute
    */
   attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'html')
         this.html = newValue;
      if (name === 'css')
         this.css = newValue;
      if (name === 'name')
         this.name = newValue;
      if (name === 'checked')
         this.checked = newValue;
   }

   //Getter for the html attribute for the custom element
   get html() {
      return this._html;
   }

   /**
    * Setter for the html attribute for the custom element, re-renders the element after change
    * 
    * @param {String} t - HTML template string
    */
   set html(t) {
      this._html = t;
      sessionStorage.setItem('html', t);
      if (this._connected)
         this.render();
   }

   //Getter for the css attribute for the custom element
   get css() {
      return this._css;
   }

   /**
    * Setter for the css attribute for the custom element
    * 
    * @param {String} x - string containing css for the custom element
    */
   set css(s) {
      this._css = s;
      sessionStorage.setItem('css', s);
      this._styleElem.textContent = s;
   }

   //Getter for the name attribute for the custom element
   get name() {
      return this._name;
   }

   /**
    * Setter for the name attribute for the custom element
    * 
    * @param {String} s - name for the name attribute to be attached to the custom element
    */
   set name(s) {
      this._name = JSON.stringify(s);
      sessionStorage.setItem('name', s);
   }

   //Getter for the checked attribute for the custom element
   get checked() {
      return this._checked;
   }

   /**
    * Setter for the checked attribute for the custom element
    * 
    * @param {Array} c - array of checked checkboxes for the custom element
    */
   set checked(c) {
      this._checked = c;
      sessionStorage.setItem('checked', c);
      if (this._connected)
         this.render();
   }

   /**
    * Function for when the checkbox is clicked 
    *
    * @param {Event} e - event object
    * @param {Element} el - the current custom element
    */
   showSlider(e, el) {
      //check if the clicked checkbox is in the array of checked checkboxes
      //if true, remove checkbox from array
      //if false, add to array
      if (el.checkedBoxes.includes(e.target.value)) {
         let index = el.checkedBoxes.indexOf(e.target.value);
         if (index !== -1) {
            el.checkedBoxes.splice(index, 1);
         }
      } else {
         el.checkedBoxes.push(e.target.value)
      }

      //send array of checked boxes to be dispathced in custom event
      this.dispatchData(el)
   }

   /**
    * Dispatch custom event with attached object of checked boxes
    * 
    * @param {Element} el - the current custom element
    */
   dispatchData(el) {
      //Set object of checked boxes to be dispatched
      let details = {
         checkboxes: el.checkedBoxes
      }

      //Dispatch event when called from event listener on wix page
      this.dispatchEvent(new CustomEvent('my-event', {
         detail: details
      }));
   }

   //Set the css and html attributes and render element
   connectedCallback() {
      let savedHtml = sessionStorage.getItem('html');
      let savedCss = sessionStorage.getItem('css');
      if (savedCss && savedCss !== 'undefined' && !this._css)
         this.css = savedCss;
      if (savedHtml && savedHtml !== 'undefined' && !this._html)
         this._html = savedHtml;
      this._connected = true;
      if (this._html) {
         this.render();
      }
   }

   //Render function for the custom element
   render() {
      this._root.innerHTML = this._html;

      this.checkedBoxes = this._checked ? this.checked.split(",") : [];

      let el = this
      let checkboxes = this._root.querySelectorAll('.input__checkbox')
      for (var i = 0; i < checkboxes.length; i++) {
         if (checkboxes.length < 6) {
            checkboxes[i].parentElement.classList.add('single_column')
         }
         checkboxes[i].addEventListener('click', e => this.showSlider(e, el), false)
      }

      //if there are pre-checked boxes check the boxes & add them to dispatched data
      if(this.checkedBoxes) {
         for (let i = 0; i < this.checkedBoxes.length; i++) {
            const element = this.checkedBoxes[i];
            for (var j = 0; j < checkboxes.length; j++) {
               if (checkboxes[j].value == element) {
                  checkboxes[j].checked = true
               }
            }
         }

         this.dispatchData(el)
      }
   }
}

//Define the checkbox element for use on wix pages, used as the element source
customElements.define('check-elm', Check);