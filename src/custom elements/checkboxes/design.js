/**
 * HTML template for the container for all the checkboxes
 * 
 * @param {Array} checkbox - Array of HTML templates of checkboxes elements to be displayed in container 
 * @param {String} name - unique name of the container
 */
export function renderCheckboxWrapper(checkbox, name) {
   return `
      <div>
         <div id="${name}Container">
            <div class="checkboxContainer" id="${name}CheckboxConatiner">
               ${checkbox.join('')}
            </div>
         </div>
      </div>`;
}

/**
 * Tag HTML template
 * 
 * @param {String} name - name of group the checkboxes belong to
 * @param {Object} item - checkbox data object 
 * @param {String} item.name - value of the checkbox
 * @param {Boolean} col - used to set the custom element in on eor two columns
 */
export function renderCheckbox(item, name, col) {
   let colClass = ''
   if(col) {
      colClass = 'single_column'
   }
   let className = `${name}Checkbox ${colClass}`

   return `
      <label class="${className}">
         <input class="input__checkbox" type="checkbox" name="${item.name}" value="${item.name}" id="${item.name}Check">
         <span class="input__checkbox--text">${item.name}</span>
      </label>`;
}

//CSS for the tag conatiner and the tag element
export const CheckboxCSS = `
   .input__checkbox--text {
      position: relative;
      padding-left: 35px;
      cursor: pointer;
      display: inline-block;
      line-height: 19px;
      user-select: none; 
      font-size: 19px;
      font-family: avenir-lt-w01_35-light1475496, avenir-lt-w05_35-light, sans-serif;
      color: #2B5672
   }

   label{
      width: 50%;
      margin-bottom: 10px;
      text-align: left;
   }

   label.single_column{
      width: 100%;
   }

   .input__checkbox--text:after {
      content: "";
      position: absolute;
      display: none; 
   }

   .input__checkbox:checked + .input__checkbox--text:after {
      display: block; 
   }

   .input__checkbox:checked + .input__checkbox--text:before {
      background-color: #05ffff69; 
   }

   .input__checkbox:disabled + .input__checkbox--text {
      cursor: initial; 
   }

   .input__checkbox:disabled + .input__checkbox--text:before {
      background-color: #ebebe4;
      border: 2px solid #ebebe4; 
   }

   .input__checkbox--text:hover:before {
      background-color: #05ffff69; 
   }

   .input__checkbox--text:after {
      left: 7px;
      top: 18%;
      width: 3px;
      height: 8px;
      border: solid #67E891;
      border-width: 0 2px 2px 0;
      transform: rotate(46deg);
   }

   .input__checkbox--text:before {
      content: '';
      left: 0;
      top: 2%;
      position: absolute;
      height: 14px;
      width: 14px;
      border: 2px solid #80EFFF;
      transition: all .2s;
      border-radius: 5px;
   }

   .input__checkbox {
      position: absolute;
      opacity: 0;
      pointer-events: none;
      width: auto; 
   }

   .checkboxContainer  {
      display: flex;
      flex-wrap: wrap;
      position: relative;
   }
`;