import Chart from '@posthog/chart.js';

class PolarElm extends HTMLElement {
   constructor() {
      super()
      this._shadow = this.attachShadow({ mode: 'open'})
      this._root = document.createElement('canvas')
      this._root.setAttribute('id', 'myChart')
      this._root.setAttribute('style', 'width: 100%')
      this._shadow.appendChild(this._root)

      this._parent = document.querySelector('polar-elm')
   }

   static get observedAttributes() {
      return ['chart-data']
   }

   attributeChangedCallback(name, oldvalue, newvalue) {
      name === 'chart-data' ? this.chartData = newvalue : null
   }

   get chartData() {
      return this._chartData
   }

   set chartData(d) {
      this._chartData = JSON.parse(d)
      sessionStorage.setItem('chartData', d)
      this._connected ? this.render() : null
   }

   connectedCallback() {
      this._parent.style.display = 'block'

      let savedData = sessionStorage.getItem('chartData')
      savedData && savedData !== 'undefined' && !this._chartData ? this.chartData = savedData : null
      this._connected = true
      this._chartData ? this.render() : null
   }

   render() {
      const ctx = this._shadow.getElementById('myChart').getContext('2d')

      const chart = new Chart(ctx, {
         type: 'polarArea',
         data: this.chartData,
         options: {
            animation: {
               animateRotate: false,
               animateScale: false
            },
            legend: {
               position: 'right'
            },
            scale: {
               ticks: {
                  min: 0,
                  max: 10,
               }
            },
            scales: {
               xAxes: []
            }
         }
      })
   }
}

window.customElements.define('polar-elm', PolarElm)