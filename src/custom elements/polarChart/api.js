import wixSeo from 'wix-seo'

export class chartPolarAPI {
   constructor(elm) {
      this.elm = elm
   }

   get data() {
      return this._data
   }

   set data(d) {
      this._data = d
      this.render()
   }

   get type() {
      return this._type
   }

   set type(t) {
      this._type = t
      this.render()
   }

   get customization() {
      return this._customization
   }

   set customization(c) {
      this._customization = c
      this.render()
   }

   render() {
      if(this.customization) {
         const { label } = this.customization
         wixSeo.setTitle = label
      }

      if(this.type) {
         this.elm.setAttribute('chart-type', this.type)
      }

      if(this.data) {
         const datasets = Object.assign({}, this.data.datasets[0], this.customization)
         this.data.datasets[0] = datasets

         this.elm.setAttribute('chart-data', JSON.stringify(this.data))
      }
   }
}