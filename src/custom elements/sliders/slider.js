class Slider extends HTMLElement {

   //constructor for the custom element
   constructor() {
      super();
      this._shadow = this.attachShadow({
         mode: 'open'
      });
      this._styleElem = document.createElement('style');
      this._styleElem.textContent = '';
      this._root = document.createElement('div');
      this._shadow.appendChild(this._styleElem);
      this._shadow.appendChild(this._root);
      //color properties for the slider
      this._sliderProps = {
         fill: "#05FFFF",
         background: "#205685",
      };
   }

   //attributes that are watched for changes
   static get observedAttributes() {
      return ['html', 'css'];
   }

   /**
    * set the new value for the watched attribute
    * 
    * @param {*} name - name of the attribute being changed
    * @param {*} oldValue - old value of the attribute
    * @param {*} newValue - new valie of the attribute
    */
   attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'html')
         this.html = newValue;
      if (name === 'css')
         this.css = newValue;
   }

   //getter for the html attribute
   get html() {
      return this._html;
   }

   /**
    * setter for the html attribute
    * 
    * @param {String} html - HTML template string
    */
   set html(html) {
      this._html = html;
      sessionStorage.setItem('html', html);
      if (this._connected)
         this.render();
   }

   //gettre for the css attribute
   get css() {
      return this._css;
   }

   /**
    * setter for the css attribute
    * 
    * @param {String} css - css for the custom element as a string
    */
   set css(css) {
      this._css = css;
      sessionStorage.setItem('css', css);
      this._styleElem.textContent = css;
   }

   /**
    * function to style the slider the correct color
    * 
    * @param {Element} slider - range input element for the current slider
    * @param {Element} sliderValue - element that holds the data-length attribute for the current slider
    */
   applyFill(slider, sliderValue) {
      const percentage = (100 * (slider.value - slider.min)) / (slider.max - slider.min);
      const bg = `linear-gradient(90deg, ${this._sliderProps.fill} ${percentage}%, ${this._sliderProps.background} ${percentage + 0.1}%)`;
      slider.style.background = bg;
      sliderValue.setAttribute("data-length", slider.value);
   }

   /**
    * function to set slider values to global data to be emitted back to the user
    * 
    * @param {Event} e - event handler
    * @param {Element} el - current custom element
    * @param {Element} sliderValue - element that holds the data-length attribute for the current slider
    */
   setSlider(e, el, sliderValue) {
      sliderValue.setAttribute("data-length", e.target.value);
      this.applyFill(e.target, sliderValue);

      el.sliders[e.target.id] = e.target.value;
      el.dispatchData(el)
   }

   /**
    * Dispatch custom event with attached object of checked checkboxes and corrospding slider values
    * 
    * @param {Element} el - the current custom element
    */
   dispatchData(el) {
      //Set object of slider values to be dispatched            
      let details = {
         sliders: el.sliders
      }

      //Dispatch event when called from event listener on wix page         
      this.dispatchEvent(new CustomEvent('my-event', {
         detail: details
      }))
   }

   //Set the css and html attributes and render element
   connectedCallback() {
      let savedHtml = sessionStorage.getItem('html');
      let savedCss = sessionStorage.getItem('css');
      if (savedCss && savedCss !== 'undefined' && !this._css)
         this.css = savedCss;
      if (savedHtml && savedHtml !== 'undefined' && !this._html)
         this.html = savedHtml;
      this._connected = true;
      if (this._html)
         this.render();
   }

   //Render function for the custom element
   render() {
      this._root.innerHTML = this._html;
      this.sliders = [];
      let el = this;
      let sliders = this._root.querySelectorAll('.range__slider');

      for (let i = 0; i < sliders.length; i++) {
         const slider = sliders[i];
         const sliderValue = slider.querySelector(".length__title");

         //adds input event listener to all the sliders in the custom element
         slider.querySelector('input').addEventListener('input', e => this.setSlider(e, el, sliderValue), false)

         let id = slider.querySelector('input').id;
         el.sliders[id] = '0';

         //adds the slider fill style
         this.applyFill(slider.querySelector("input"), sliderValue);

         // adds in the visual notches to the custom element
         const notch = parseInt(slider.querySelector("input").max) + 1
         for (let i = 0; i < notch; i++) {
            const el = document.createElement('span');
            const width = i * (100 / (notch - 1)) + '%';

            //sets the correct position for every notch
            el.style.left = width;

            slider.querySelector('.notch').appendChild(el)
         }
      }
   }
}

//Define the slider element for use on wix pages, used as the element source
customElements.define('slider-ele', Slider)