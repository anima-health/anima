/**
 * Slider HTML template
 * 
 * @param {String} name - name of group the sliders belong to
 * @param {Object} item - slider data object 
 * @param {String} item.name - value of the slider
 */
export function renderSlider(item, name) {
   return `
      <div class="length range__slider" data-min="0" data-max="10">
         <div class="length__title field-title" data-length='0'></div>
         <input id="${item.name}" class="slider" type="range" step='1' min="0" max="10" value="0" />
         <span class="notch"></span>
      </div>`
}

/**
 * HTML template for the custom element container
 * 
 * @param {Array} slider - array of HTML templates for the sliders
 * @param {String} name - ID for the container
 */
export function renderSliderWrapper(slider, name) {
   return `
      <div>
         <div id="${name}Container">
            <div id="${name}TagConatiner">
               ${slider.join('')}
            </div>
         </div>
      </div>`
}

//CSS for the custom element
export const SliderCSS = `
   .range__slider {
      position: relative;
      width: 100%;
      height: 0;
      display: flex;
      justify-content: center;
      align-items: center;
      border-radius: 8px;
      margin: 0;
   }

   .slider {
      -webkit-appearance: none;
      width: calc(100% - (70px));
      height: 7px;
      border-radius: 5px;
      background: rgba(255, 255, 255, 0.314);
      outline: none;
      padding: 0;
      margin: 0;
      cursor: pointer;
      background: linear-gradient(90deg, rgb(4, 230, 136) 10%, rgb(32, 86, 133) 10.1%);
   }

   .slider::-webkit-slider-thumb {
      -webkit-appearance: none;
      width: 25px;
      height: 25px;
      border-radius: 50%;
      background: #1DABBF;
      cursor: pointer;
      transition: all 0.15s ease-in-out;
   }

   .slider::-webkit-slider-thumb:hover {
      background: #59b678;
      transform: scale(1.2);
   }

   .slider::-moz-range-thumb {
      width: 20px;
      height: 20px;
      border: 0;
      border-radius: 50%;
      background: #67E891;
      cursor: pointer;
      transition: background 0.15s ease-in-out;
   }

   .slider::-moz-range-thumb:hover {
      background: #59b678;
   }

   .notch {
      width: calc(100% - (88px));
      height: 10px;
      position: absolute;
      bottom: 0;
   }

   .notch span {
      display: inline-block;
      width: 2px;
      height: 10px;
      position: absolute;
      background: #205685;
      top: 0;
   }
`