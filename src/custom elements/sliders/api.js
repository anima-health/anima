export class SliderAPI {
   /**
    * Constructor for the custom element
    * 
    * @param {Element} el - the current custom element
    * @param {String} globalTemp - HTML template string for the custom element container
    * @param {String} sliderTemp  - HTML template string for the slider
    * @param {String} name - identifier for the custom element
    */
   constructor(el, globalTemp, sliderTemp, name) {
      this.el = el
      this._globalTemp = globalTemp
      this._sliderTemp = sliderTemp
      this._name = name
   }

   //getter for the css attribute
   get css() {
      return this._css
   }

   /**
    * setter for the css attribute
    * 
    * @param {String} css - css to be applied to the custom element as a string
    */
   set css(css) {
      this._css = css
      this.el.setAttribute('css', css)
   }

   //getter for the data attribute
   get data() {
      return this._data
   }

   /**
    * setter for the data attribute
    * 
    * @param {Array} data - array of data to be used in the custom element
    */
   set data(data) {
      this._data = data
      this.render()
   }

   //Render the custom element by setting the HTML templates to the HTML attribute
   render() {
      let template = this._data.map(_ => this._sliderTemp(_, this._name))
      let html = this._globalTemp(template, this._name)
      this.el.setAttribute('html', html)
   }
}