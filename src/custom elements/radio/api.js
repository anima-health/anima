//API
export class RadioAPI {
   /**
    * Constructor for the radioBTN custom element
    * 
    * @param {String} el - element ID that the custom element with be inserted into
    * @param {String} globalTemp - HTML template for the custom element container
    * @param {String} radioTemp - HTML template for the actual radio element
    * @param {String} name - identifier sent to the custom element
    * @param {Boolean} col - boolean to set the element in one column or two
    */
   constructor(el, globalTemp, radioTemp, name, col = false) {
      this.el = el
      this._globalTemp = globalTemp
      this._radioTemp = radioTemp
      this._name = name
      this._col = col
   }

   //getter for the css attribute for the custom element
   get css() {
      return this._css;
   }

   /**
    * setter for the css attribute for the custom element
    * 
    * @param {String} css - string containing css for the custom element
    */
   set css(css) {
      this._css = css;
      this.el.setAttribute('css', css);
   }

   //getter for the data attribute for the custom element
   get data() {
      return this._data;
   }

   /**
    * setter for the data attribute for the custom element
    * 
    * @param {Array} d - array of the data for the custom element
    */
   set data(d) {
      this._data = d;
      this.render();
   }

   //render function for the custom element, sets the HTML templates as the HTML attribute for the element
   render() {
      let radioTemp = this._data.map(_ => this._radioTemp(_, this._name, this._col));
      let html = this._globalTemp(radioTemp, this._name);
      this.el.setAttribute('html', html);
      this.el.setAttribute('name', this._name);
   }
}