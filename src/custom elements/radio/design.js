/**
 * HTML template for the container for all the radio buttons
 * 
 * @param {Array} radio - Array of HTML templates of radio button elements to be displayed in container 
 * @param {String} name - unique name of the container
 */
 export function renderRadioWrapper(radio, name) {
   return `<div>
               <div id="${name}Container">
                   <div class="radioContainer" id="${name}RadioConatiner">
                       ${radio.join('')}
                   </div>
               </div>
           </div>`;
}

/**
 * Radio button HTML template
 * 
 * @param {String} name - name of group the radio button belong to
 * @param {Object} item - radio data object 
 * @param {String} item.name - value of the radio button
 * @param {Boolean} col - used to set the custom element in on eor two columns
 */
export function renderRadio(item, name, col) {
   let colClass = ''
   if(col) {
      colClass = 'single_column'
   }
   let className = `${name}Radio ${colClass}`

   return `<label class="${className}">
               <input class="input__radio" type="radio" name="${name}" value="${item.name}" id="${item.name}Radio">
               <span class="input__radio--text">${item.name}</span>
           </label>`;
}

//CSS for the tag conatiner and the radio btn element
export const RadioCSS = `.input__radio--text {
   position: relative;
   padding-left: 35px;
   cursor: pointer;
   display: inline-block;
   line-height: 25px;
   user-select: none;  
   font-family: avenir-light;
   font-size: 19px;
   color: #2d5671;
}

label{
   width: 50%;
   margin-bottom: 10px;
   text-align: left;
}

label.single_column{
   width: 100%;
}

.input__radio--text:after {
   content: "";
   position: absolute;
   display: none; 
}

.input__radio:checked + .input__radio--text:after {
   display: block; 
}

.input__radio:checked + .input__radio--text:before {
   background-color: #05ffff69; 
}

.input__radio:disabled + .input__radio--text {
   cursor: initial; 
}

.input__radio:disabled + .input__radio--text:before {
   background-color: #ebebe4;
   border: 2px solid #ebebe4; 
}

.input__radio--text:hover:before {
   background-color: #05ffff69; 
}

.input__radio--text:after {
   left: 6px;
   top: 6px;
   width: 12px;
   height: 12px;
   border-radius: 50%;
   background: #67E891;
}

.input__radio--text:before {
   content: '';
   left: 0;
   position: absolute;
   height: 20px;
   width: 20px;
   border: 2px solid #05FFFF;
   transition: all .2s;
   border-radius: 50%;
}

.input__radio {
   position: absolute;
   opacity: 0;
   pointer-events: none;
   width: auto; 
}

.radioContainer  {
   display: flex;
   flex-wrap: wrap;
   position: relative;
}`;