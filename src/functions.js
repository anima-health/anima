import wixData from 'wix-data';
import wixUsers from 'wix-users';
import wixLocation from 'wix-location';

import { chartPolarAPI } from 'public/polar/api.js'

import { chartJSAPI } from 'public/chart/api.js';

import { TagAPI } from 'public/tag/api.js'
import { renderTag, renderTagWrapper, TagCSS } from 'public/tag/design.js'

import { RadioAPI } from 'public/radio/api.js'
import { renderRadio, renderRadioWrapper, RadioCSS } from 'public/radio/design.js'

import { CheckboxAPI } from 'public/checkbox/api.js'
import { renderCheckbox, renderCheckboxWrapper, CheckboxCSS } from 'public/checkbox/design.js'

import { renderChecksliderCheck, renderChecksliderSlider, renderChecksliderWrapper, ChecksliderCSS } from 'public/checkboxSlider/design.js'
import { CheckSliderAPI } from 'public/checkboxSlider/api.js';

/**
 * gets URL param and assigns it to dropdown value
 * 
 * @param {Element} el - dropdown element that the param is assigned to
 * @param {String} param - name of the param in the URL
 * @param {Function} callback - function to run after dropdown value is set
 */
export function dropdownGetParam(el, param, callback) {
   if (wixLocation.query[`${param}`]) {
      let query = wixLocation.query;
      el.value = query[`${param}`];
      callback();
   }
}

/**
 * inserts data into dateset for data only allowed once a day per user
 * 
 * @param {String} dataset - dataset the data is to be written to
 * @param {Object} data - data to write to dataset
 */
export async function insertDataDaily(dataset, data) {
   let alreadyLogged = false;
   
   //check the dataset to see if a record with the current user and current date exists
   await wixData.query(dataset)
      .eq("date", getDate())
      .eq("uid", data.uid)
      .find()
      .then((res) => {
         //if there is a result the user has entered data on the current date
         if (res.items.length > 0) {
            alreadyLogged = true;
         }
      });

   //if the user has not entered data on the current date insert it into dataset
   if (!alreadyLogged) {
      wixData.insert(dataset, data)
         .then((results) => {
            console.log('data entered!')
         })
         .catch((err) => {
            let errorMsg = err;
         });
   //the user has already inserted data on the current date so do nothing
   } else {
      console.log('todays data already logged')
   }
}

/**
 * inserts data into dateset for data
 * 
 * @param {String} dataset - dataset the data is to be written to
 * @param {String} data - data to write to dataset
 */
export async function insertData(dataset, data) {
   wixData.insert(dataset, data)
      .then((res) => {
         console.log('data entered!')
      })
      .catch((err) => {
         let errorMsg = err;
      });
}

/**
 * Update a record in a dataset
 * 
 * @param {String} dataset - name of dataset for data to be updated in
 * @param {Array} data - array of data that is being updated
 * @param {String} id - id of record in dataset
 */
export async function updateData(dataset, data , id){
   data._id = id
   wixData.update(dataset, data)
	.then((res) => {
      console.log('data updated!')
	} )
	.catch((err) => {
		let errorMsg = err;
	} );
}

/**
 * function to create a unique ID for dataset entry
 * 
 * @param {String} dataset - name of dataset to query
 * @returns string of numbers to be used as ID
 */
export async function createUniqueID(dataset) {
   let len = 1
   let hash = 17
   let hex, oct

   //get the number of rows in the dataset
   await wixData.query(dataset)
   .find()
   .then((res) => {
      len = res.items.length + 1
   });
   
   //create a hash based on the number of rows
   for (i = 0; i < len.toString().length; i++) {
      char = len.toString().charCodeAt(i)
      hash = ((hash<<5)-hash)+char
      hash = hash & hash // Convert to 32bit integer
   }

   //convert the hash to both hexadecimal and octadecimal values
   oct = hash.toString(8)
   hex = hash.toString(16)

   //return string of combined hex and oct number
   return `${oct}_${hex}`
}

/**
 * generates date in dd/mm/yyyy format
 */
export function getDate(date = new Date()) {
   //let today = new Date();
   let dd = date.getDate();
   let mm = date.getMonth() + 1;
   let yyyy = date.getFullYear();

   //adds leading zero onto day & month if they are below 10; e.g. 2 -> 02
   dd < 10 ? dd = `0${dd}` : ''
   mm < 10 ? mm = `0${mm}` : ''

   date = `${dd}/${mm}/${yyyy}`
   return date
}

/**
 * generates tag custom elements from list of arguments
 * 
 * @param {Object} tagItem - Object for custom tag element
 * @param {String} tagItem.element - id of custome element
 * @param {String} tagItem.dataset - dataset to be used for the tags
 * @param {String} tagItem.field - field key in the dataset to use for the options
 * @param {String} tagItem.name - identifier sent to the custom element
 * @param {Array} tagItem.data - additional options for the custom element
 */
export async function getTags(tagItem) {
   // Creates the custom tag element HTML skeleton
   let tag = new TagAPI($w(`${tagItem.element}`), renderTagWrapper, renderTag, `${tagItem.name}`);

   // Gets the data for the custom element
   let data = await getData(`${tagItem.dataset}`, `${tagItem.field}`);

   if (tagItem.data) {
      data = await insertAdditional(data, tagItem.data)
   }

   // Sets the data and css attributes on the custom elements
   setCustomElement(tag, TagCSS, data);
}

/**
 * generates radioBTN custom elements from list of arguments
 * 
 * @param {Object} radioItem - Object for custom checkbox element
 * @param {String} radioItem.element - id of custome element
 * @param {String} radioItem.dataset - dataset to be used for the checkboxes
 * @param {String} radioItem.field - field key in the dataset to use for the options
 * @param {String} radioItem.name - identifier sent to the custom element
 * @param {Boolean} radioItem.single - wheather the checkboxes should be a single column
 * @param {Array} radioItem.data - additional options for the custom element
 */
export async function getRadio(radioItem) {
   // Creates the custom checkbox element HTML skeleton
   let radio = new RadioAPI($w(`${radioItem.element}`), renderRadioWrapper, renderRadio, `${radioItem.name}`, radioItem.single);
   // Gets the data for the custom element
   let data = await getData(`${radioItem.dataset}`, `${radioItem.field}`);

   if (radioItem.data) {
      data = await insertAdditional(data, radioItem.data)
   }

   // Sets the data and css attributes on the custom elements
   setCustomElement(radio, RadioCSS, data);
}

/**
 * generates checkbox custom elements from list of arguments
 * 
 * @param {Object} checkItem - Object for custom checkbox element
 * @param {String} checkItem.element - id of custome element
 * @param {String} checkItem.dataset - dataset to be used for the checkboxes
 * @param {String} checkItem.field - field key in the dataset to use for the options
 * @param {String} checkItem.name - identifier sent to the custom element
 * @param {Boolean} checkItem.single - wheather the checkboxes should be a single column
 * @param {Array} checkItem.data - additional options for the custom element
 */
export async function getCheckboxes(checkItem) {
   // Creates the custom checkbox element HTML skeleton
   let check = new CheckboxAPI($w(`${checkItem.element}`), renderCheckboxWrapper, renderCheckbox, `${checkItem.name}`, checkItem.single);
   // Gets the data for the custom element
   let data = await getData(`${checkItem.dataset}`, `${checkItem.field}`);

   if (checkItem.data) {
      data = await insertAdditional(data, checkItem.data)
   }
   // Sets the data and css attributes on the custom elements
   setCustomElement(check, CheckboxCSS, data);
   
   return check
}

/**
 * generates checkbox with slider custom elements from list of arguments
 * 
 * @param {Object} item - Object for custom checkbox element
 * @param {String} item.element - id of custome element
 * @param {String} item.dataset - dataset to be used for the checkboxes
 * @param {String} item.field - field key in the dataset to use for the options
 * @param {String} item.name - identifier sent to the custom element
 * @param {Array} item.data - additional options for the custom element
 */
export async function getCheckSliders(item) {
   // Creates the custom checkbox element HTML skeleton
   let check = new CheckSliderAPI($w(`${item.element}`), renderChecksliderWrapper, renderChecksliderCheck, renderChecksliderSlider, `${item.name}`)

   // Gets the data for the custom element
   let data = await getData(`${item.dataset}`, `${item.field}`);
   
   if (item.data) {
      data = await insertAdditional(data, item.data)
   }

   // Sets the data and css attributes on the custom elements
   setCustomElement(check, ChecksliderCSS, data)

   //return API so that the data attribute can be altered
   return check
}

/**
 * sets the custom elemenst css & data attributes
 * 
 * @param {Element} el - custom element
 * @param {String} css - css const from custom element
 * @param {Array} data - data sent into the custom element
 */
export function setCustomElement(el, css, data) {
   el.css = css;
   el.data = data;
}

/**
 * Adds any other options to be used in the custom element
 * 
 * @param {Array} curr - Current data to be used in custom element
 * @param {Array} data - Array of additional data to be added
 * @returns merged array of curr and data
 */
export async function insertAdditional(curr, data) {
   let hasOther = false
   let hasDontKnow = false
   
   //loops through all options in the data array
   for (let i = 0; i < data.length; i++) {
      //checks if the 'other' or the 'i dont know' option exists
      if (data[i] == 'Other') {
         hasOther = true
      } else if (data[i] == "I Don't Know") {
         hasDontKnow = true
      } else {
         //sorts all other options in alphabetical order
         curr.push({
            name: data[i]
         })
         curr.sort(nameSort)
      }
   }

   //if 'other' option exists add it to the array
   if(hasOther) {
      curr = addOther(curr)
   }

   //if 'i dont know' option exists add it to the array
   if(hasDontKnow) {
      curr = addIDontKnow(curr)
   }

   //return the sorted array 
   return curr
}

/**
 * adds the 'i dont know' option to the start of the array
 * 
 * @param {Array} arr - array of custom element options
 * @returns array of custom element options
 */
export function addIDontKnow(arr) {
   //add the 'i dont know' option to the start of the array
   arr.unshift({
      name: "I Don't Know"
   })

   return arr
}

/**
 * adds the 'other' option to the end of the array
 * 
 * @param {Array} arr - array of custom element options
 * @returns array of custom element options
 */
export function addOther(arr) {
   //add the 'other' option to the end of the array
   arr.push({
      name: "Other"
   })

   return arr
}

/**
 * simple sort array function, used to sort array by the name property
 */
function nameSort(a, b) {
   let nameA = a.name
   let nameB = b.name
   if (nameA < nameB) {
      return -1;
   }
   if (nameA > nameB) {
      return 1;
   }

   // names must be equal
   return 0;
}

/**
 * get data from dataset to be used for custom elements
 * 
 * @param {String} dataset - name of dataset the data is in
 * @param {String} field - name of the field in the dataset
 */
export async function getData(dataset, field) {
   let arr = []

   //gets data from dataset
   let data = await wixData.query(`${dataset}`).find();

   //map data from specfic field to a new array
   let newData = data.items.map(item => {
      return {
         name: item[`${field}`],
      }
   });

   //remove any 'undefined' entries in the array
   for (let i = 0; i < newData.length; i++) {
      if (newData[i].name !== undefined) {
         arr.push(newData[i])
      }
   }

   //make sure data is only strings and not array ( caused by dataset field being a tag )
   for (let i = 0; i < arr.length; i++) {
      const element = arr[i];
      if(Array.isArray(element.name)) {
         element.name[0] = element.name[0].replaceAll('"', '')
         arr[i].name = element.name[0]
      }
   }

   return arr;
}

/**
 * get data and filter it by value from dataset to be used for custom elements
 * 
 * @param {String} dataset - name of dataset the data is in
 * @param {String} field - name of the field in the dataset
 * @param {String} filterKey - name of the field to filter results by
 * @param {String} filterValue - value of filterkey to filter by
 */
export async function getDataFilter(dataset, field, filterKey, filterValue) {
   let arr = []
   // Gets data from dataset where specific field has a specific value
   let data = await wixData.query(`${dataset}`)
      .contains(filterKey, filterValue)
      .find();

   // Map data from specfic field to a new array
   let newData = data.items.map(item => {
      return {
         name: item[`${field}`],
      }
   });

   // Remove any 'undefined' entries in the array
   for (let i = 0; i < newData.length; i++) {
      if (newData[i].name !== undefined) {
         arr.push(newData[i])
      }
   }

   return arr;
}

/**
 * get data from dataset to be used for custom elements, if dataset field has an array gets the array out
 * 
 * @param {String} dataset - name of dataset the data is in
 * @param {String} field - name of the field in the dataset
 */
export async function getDataArr(dataset, field) {
   let arr = []
   
   //gets data from dataset
   let data = await wixData.query(`${dataset}`).find();
   
   //map data from specfic field to a new array
   let newData = data.items.map(item => {
      return {
         name: item[`${field}`],
      }
   });

   //remove any 'undefined' entries in the array
   for (let i = 0; i < newData.length; i++) {
      if (newData[i].name !== undefined) {
         arr.push(newData[i])
      }
   }

   //if field contains an array map it to a new array to be returned
   let finalData = arr[0].name.map(item => {
      return {
         name: item,
      }
   });

   return finalData;
}

/**
 * gets list of options for drop down
 * 
 * @param {Object} args - Object of query argumnets
 * @param {String} args.element - id of current dropdown
 * @param {String} args.dataset - dataset to be used for drop down
 * @param {String} args.key - field key in the dataset to use for the options
 * @param {String} args.options - field key in the dataset to use for the options
 */
export function getDropDownItems(args, user = null) {
   //get data from dataset
   if(user == null) {
      wixData.query(args.dataset)
         .limit(1000)
         .find()
         .then(results => {
            //set drop down options based on specfic field
            setDropdown(results.items, args.key, args.element)
         });
   } else {
      wixData.query(args.dataset)
         .contains('userId', user.id)
         .limit(1000)
         .find()
         .then(results => {
            setDropdown(results.items, args.key, args.element)
         });
   }
}

/**
 * gets list of options for drop down filtered on one other dropdown
 * 
 * @param {Object} args - Object of query argumnets
 * @param {String} args.element - id of current dropdown
 * @param {String} args.dataset - dataset to be used for drop down
 * @param {String} args.key - field key in the dataset to use for the options
 * @param {String} args.filter_element - id of drop down to filter against
 * @param {String} args.filter_key - field key in dataset to filter against
 * @param {Object} args.options - field key in dataset to filter against
 * @param {Boolean} args.options.sort - field key in dataset to filter against
 */
export function dropDownFilter(args) {
   // Return if the element to filter the data by is empty
   if ($w(args.filter_element).value === '') {
      return;
   }
   //get data from dataset where the specfic field is equal to the element value
   wixData.query(args.dataset)
      .contains(args.filter_key, $w(args.filter_element).value)
      .limit(1000)
      .find()
      .then(results => {
         //set drop down options based on specfic field
         setDropdown(results.items, args.key, args.element)
      });
}

/**
 * gets list of options for drop down filtered on two other dropdown
 * 
 * @param {Object} args - list of argumnets
 * @param {String} args.element - id of current dropdown
 * @param {String} args.dataset - dataset to be used for drop down
 * @param {String} args.key - field key in the dataset to use for the options
 * @param {String} args.filter_element - id of drop down to filter against
 * @param {String} args.filter_key - field key in dataset to filter against 
 * @param {String} args.filter_element_2 - id of second drop down to filter against
 * @param {String} args.filter_key_2 - second field key in dataset to filter against
 * @param {Object} args.options - field key in dataset to filter against
 * @param {Boolean} args.options.sort - field key in dataset to filter against
 */
export function dropDownDoubleFilter(args) {
   // Return if the element to filter the data by is empty
   if ($w(args.filter_element).value === '' || $w(args.filter_element_2).value === '') {
      return;
   }
   //get data from dataset where two specfic field are equal to the corropsonding element value
   wixData.query(args.dataset)
      .contains(args.filter_key, $w(args.filter_element).value)
      .contains(args.filter_key_2, $w(args.filter_element_2).value)
      .limit(1000)
      .find()
      .then(results => {
         //set drop down options based on specfic field
         setDropdown(results.items, args.key, args.element)
      });
}

/**
 * sets dropdown element with options and adds an 'i dont know' & empty line
 * 
 * @param {Array} items - list of dropdown options
 * @param {String} key - field key in the dataset to use for the options
 * @param {Element} el - dropdown element
 */
export function setDropdown(items, key, el) {
   //removes any duplicate items in the items array
   const uniqueTitles = getUniqueTitles(items, key);

   //Adds an 'i dont know option to the array'
   uniqueTitles.unshift("I Don't Know")
   
   //Adds a blank option as the first array option, used as a blank default option
   uniqueTitles.unshift("")

   //sets the dropdown options
   $w(el).options = buildOptions(uniqueTitles);
}

/**
 * gets all unique results from dataset query
 * 
 * @param {Array} items - array of items from dataset query
 * @param {String} key - name of the field to return unique values for
 */
export function getUniqueTitles(items, key) {
   //maps specfic field from items array to new array
   const titlesOnly = items.map(item => item[key]);

   //removes any undefined items in array
   let arr = [...new Set(titlesOnly)]

   for (let i = 0; i < arr.length; i++) {
      if (arr[i] === undefined) {
         arr.splice(i, 1);
      }
      if (arr[i] == '') {
         arr.splice(i, 1);
      }
   }
   //returns array of unique values
   return arr.sort();
}

/**
 * turns array in to object for drop down options
 * 
 * @param {Array} uniqueList - array of options for the dropdown
 */
export function buildOptions(uniqueList) {
   //Converts an Array into an Object to be used as options for a dropdown
   return uniqueList.map(curr => {
      return {
         label: curr,
         value: curr
      };
   });
}

/**
 * function to expand or collapse elements based upon value of input element
 * 
 * @param {Event} $event - event object that triggers the event
 * @param {String} $el - element ID for the element to be expanded or collapsed
 * @param {String} $el2 - element ID for the element to be expanded or collapsed
 */
export function exapandCollapse($event, $el, $el2) {
   $event.target.value == 'Yes' ? $w(`${$el}`).expand() : $w(`${$el}`).collapse()
   $el2 ? $event.target.value == 'Yes' ? $w(`${$el2}`).expand() : $w(`${$el2}`).collapse() : null
}

/**
 * function to creat a bar chart for daily data for a week
 * 
 * @param {Object} obj - Object for custom chart element
 * @param {String} obj.element - id of custome element
 * @param {String} obj.type - type of the chart
 * @param {Object} obj.options
 * @param {Array} obj.data
 * @param {Array} obj.labels
 * @param {Number} obj.tension
 */
export async function createBarLine(obj, repeaterItem = false) {
   //creates chart custom element
   let chart
   if(repeaterItem) {
      chart = new chartJSAPI(repeaterItem(`${obj.element}`));
   } else {
      chart = new chartJSAPI($w(`${obj.element}`));
   }

   if (obj.tension) {
      for (let i = 0; i < obj.data.length; i++) {
         obj.data[i].lineTension = obj.tension;
      }
   }

   //set the charts labels and data
	chart.data = {
		labels: obj.labels,
		datasets: obj.data
	}

   //set chart type
   obj.type ? chart.type = obj.type : null
}

/**
 * function to creat a bar chart for daily data for a week
 * 
 * @param {Object} obj - Object for custom chart element
 * @param {String} obj.element - id of custome element
 * @param {String} obj.dataset - dataset to be used for the chart
 * @param {String} obj.type - type of the chart
 * @param {Object} obj.options
 */
export async function MonthChart(obj) {
   //creates chart custom element
	let chart = new chartJSAPI($w(`${obj.element}`));

   //query the dataset for the data
	const chartItems = await wixData
		.query(obj.dataset)
		.find()
		.then(res => res.items);

	const data = chartItems.map(item => item.data);
	const backgroundColor = chartItems.map(item => item.backgroundColor);
	const labels = chartItems.map(item => item.label);

	let dataset = []

   //creates array for the data options for the cahrt
	for(let i = 0; i < data.length; i++) {
		dataset[i] = { 
         fill: 'false', 
         data: data[i], 
         label: labels[i], 
         backgroundColor: backgroundColor[i],
         borderColor: backgroundColor[i],
         lineTension: 0.3,
      }
	}

   //set the charts labels and data
	chart.data = {
		labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		datasets: dataset
	}

   //set chart tyoe
   obj.type ? chart.type = obj.type : null
}

/**
 * 
 * @param {Object} obj - Object for custom chart element
 * @param {String} obj.element - custom element ID
 * @param {Array} obj.data - data to be displayed in polar chart
 * @param {Array} obj.labels - array of lables for the data to be displayed
 */
export function createPolar(obj) {
   //creates chart custom element
   let Polarchart = new chartPolarAPI($w(`${obj.element}`));
   Polarchart.type = 'polarArea'

   //set the charts labels and data
   Polarchart.data = {
      datasets: obj.data,
      labels: obj.labels,
   }
}

/**
 * function to return weekly data filled upto a week
 * e.g. data for mon - wed passed in; will return full week with thurs - sun as blank data
 * 
 * @param {Array} data - array of data for a weekly chart
 * @returns array of data for the chart with blank data to fill upto a week
 */
export function chartDataFillWeek(data) {
   let days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
   let pos

   //remove any data before a Monday ( start of the week )
   for (let i = 0; i < data.length; i++) {
      if (data[i].date == 'Monday') {
         pos = i
      }
   }
   data.splice(0, pos)

   //get keys for chart data
   let keys = Object.keys(data[0])
   let nulldata = []
   for (let i = 0; i < keys.length; i++) {
      const element = keys[i];
      if(element != 'date') {
         nulldata.push(element)
      }
   }

   let returnArr = []
   for (let i = 0; i < days.length; i++) {
      const day = days[i];
      let obj = {
         'date': day
      }

      for (let j = 0; j < nulldata.length; j++) {
            const element = nulldata[j];
            obj[element] = null
      }

      let newData = obj

      for (let j = 0; j < data.length; j++) {
         const el = data[j];
         if (el.date == day){
            newData = el
         }
      }
      returnArr.push(newData)
   }

   //return data for the current week includeing blanks for days not yet passed
   return returnArr
}

/**
 * Function to get data for current day to be displayed for polar chart
 * 
 * @param {Array} data - array of data from dataset to be cleaned for polar chart
 * @returns data in an array ready to be sent to polar chart  
 */
export function getTodayData(data) {
   let todayData
   
   //get data for current day
   for (let i = 0; i < data.length; i++) {
      const element = data[i];
      
      if (element.daysAgo == 0) {
         todayData = element
      }
   }
   return todayData
}

/**
 * Function that averages multiple days worth of data to be displayed on a polar chart
 * 
 * @param {Array} data - array of data for a polar chart
 * @param {Number} days - number of days to get data for
 * @returns array of data to be used for polar chart
 */
export function getMultiDayData(data, days) {
   let totalhData
   let totalDay = 0

   //sort data by date
   data = data.sort(daySort)

   //gets total of all values for days
   for (let i = 0; i < data.length; i++) {
      const element = data[i];
   
      if (element.daysAgo <= days) {
         totalDay++
         if (i == 0){
            totalhData = element
         } else {
            for (let key in totalhData) {
               totalhData[key] += element[key]
            }
         } 
      }
   }
   
   //average out the data
   for (let key in totalhData) {
      totalhData[key] = totalhData[key] / totalDay
   }
   
   //send array of data back
   return totalhData
}

/**
 * Sort funciton for object with daysAgo proptery
 * 
 * @param {Object} a - object to sort
 * @param {Object} b - object to sort
 */
function daySort(a, b) {
   let nameA = a.daysAgo
   let nameB = b.daysAgo
   if (nameA < nameB) {
      return -1;
   }
   if (nameA > nameB) {
      return 1;
   }

   return 0;
}

/**
 * function to convert UK date to US date format
 * @param {Date} date - date to be converted
 * @returns date in mm/dd/yyyy format
 */
export function convertDate(date) {
   let dateParts = date.split("/");
   return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
}

/**
 * function to get the days between 2 dates  
 * 
 * @param {Date} date1 - most recent date
 * @param {Date} date2 - further away date
 * @returns number of days between dates
 */
export function daysDiff(date1, date2) {
   let diff = date1 - date2
   return ((((diff / 1000) / 60) / 60) / 24)
}

/**
 * Funciton to retreive the day on the week from a getDay() result
 * @param {Number} num - number that represents a day on the week
 * @returns string of day name
 */
export function getDay(num) {
   switch (num) {
      case 0:
         return 'Sunday'
      case 1:
         return 'Monday'
      case 2:
         return 'Tuesday'
      case 3:
         return 'Wednesday'
      case 4:
         return 'Thursday'
      case 5:
         return 'Friday'
      case 6:
         return 'Saturday'

   }
}