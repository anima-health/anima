const uuid = require('uuid');
import wixData from 'wix-data';
import wixUsers from 'wix-users';
import { createBarLine, getDate } from 'public/functions.js';
import * as chartJS from "public/charts.js";

$w.onReady(async function() {
   let itemObj = $w("#dynamicDataset").getCurrentItem();
   let arr = itemObj.statsObj

   if (arr == undefined) {
      $w('#repeater1').collapse()
   } else {
      let data = arr.map((item) => {
         item._id = uuid.v4();
         return item;
      });

      $w('#repeater1').data = data

      $w("#repeater1").onItemReady(async ($item, itemData, index) => {
         await setDailyTasks($item, itemData)

         $item('#text86').text = itemData.title
         $item('#button1').link = itemData.link
         $item('#button1').label = `To ${itemData.title}`

         let morningData = await chartJS[`${itemData.chart}`]();

         //create custom bar chart from data
         let args = {
            element: '#customElement1',
            data: morningData.dataset,
            labels: morningData.labels,
            type: 'line'
         }

         createBarLine(args, $item)
      })
   }

});

export async function setDailyTasks($item, itemData) {
   await wixData.query(`${itemData.dataset}`)
      .contains('_owner', wixUsers.currentUser.id)
      .contains('date', getDate())
      .limit(1)
      .find()
      .then(res => {
         if (res.items.length == 0) {
            $item(`#customElement1`).collapse()
            $item(`#noDataContainer`).expand()
         } else {
            $item(`#customElement1`).expand()
            $item(`#noDataContainer`).collapse()
         }
      })
}