import wixData from 'wix-data';
import wixUsers from 'wix-users';
import { createBarLine, getDate } from 'public/functions.js';
import { getMorningChartData, getEveningChartData, getLifestyleChartData } from "public/charts.js";

$w.onReady(async function () {

   let dailyData = [
      {
         dataset: 'MorningHealthCheckResponseDB',
         container: '#morningDataMissingContainer',
         chart: '#morningChart'
      }, {
         dataset: 'EveningHealthCheckResponseDB',
         container: '#eveningDataMissingContainer',
         chart: '#eveningChart',
      }, {
         dataset: 'LifestyleHealthCheckResponseDB',
         container: '#lifestyleDataMissingContainer',
         chart: '#lifeStyleChart',
      }
   ]

   await setDailyTasks(dailyData)

   //get data for mroning wellness chart
   let morningData = await getMorningChartData()

   //create custom bar chart from data
   let M_args = {
      element: '#morningChart',
      data: morningData.dataset,
      labels: morningData.labels,
      type: 'line'
   }
   createBarLine(M_args)

   //get data for evening wellness chart
   let eveningData = await getEveningChartData()

   //create custom bar chart from data
   let E_args = {
      element: '#eveningChart',
      data: eveningData.dataset,
      labels: eveningData.labels,
      type: 'line'
   }
   createBarLine(E_args)

   //get data for evening wellness chart
   let lifestyleData = await getLifestyleChartData()

   //create custom bar chart from data
   let L_args = {
      element: '#lifeStyleChart',
      data: lifestyleData.dataset,
      labels: lifestyleData.labels,
      type: 'line'
   }
   createBarLine(L_args)
});

export async function setDailyTasks(items) {
   for (let i = 0; i < items.length; i++) {
      const element = items[i];

      await wixData.query(`${element.dataset}`)
      .contains('_owner', wixUsers.currentUser.id)
      .contains('date', getDate())
      .limit(1)
      .find()
      .then(res => {
         if (res.items.length != 0) {
            $w(`${element.container}`).collapse()
            $w(`${element.chart}`).expand()
            return false
         } else {
            $w(`${element.container}`).expand()
            $w(`${element.chart}`).collapse()
            return true
         }
      })
   }
}