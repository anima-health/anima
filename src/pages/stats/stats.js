import wixLocation from 'wix-location';
import wixData from 'wix-data';
import wixUsers from 'wix-users';
import { createBarLine, MonthChart, createPolar } from 'public/functions.js';
import { getEveningChartData, getMorningChartData, getPolarChartData } from "public/charts.js";

//range for polar chart data
let range = 'day'
let barRange = 'week'

$w.onReady(async function () {
   let tableActivity = []
   let tableIssue = []

   //query to get all the current users Health Issue Jounrals to be displayed in table
   wixData.query('HealthIssueJournalResponseDB')
      .contains('userId', wixUsers.currentUser.id)
      .limit(1000)
      .find()
      .then(results => {
         //if there are no results dont show the conatiner for the table
         if (results.items && results.items.length > 0) {
            //$w('#box18').expand()
         } else {
            //$w('#box18').collapse()
         }

         //loop through each journal and create tale row
         for (var i = 0; i < results.items.length; i++) {
            let data = {
               healthIssueName: results.items[i].healthIssueName,
               _createdDate: results.items[i]._createdDate,
               healthIssueJournalReference: `<a href='https://lbdesignsofficial.wixsite.com/mysite/account-1/blank-1?testParam=${results.items[i].healthIssueJournalReference}'>${results.items[i].healthIssueJournalReference}</a>`
            }
            tableIssue.push(data);
         }

         //set table rows
         $w('#table1').rows = tableIssue;
      });

   //query to get all the current users Health Activity Journals to be displayed in a table
   wixData.query('HealthActivityJournalResponseDB')
      .contains('userId', wixUsers.currentUser.id)
      .limit(1000)
      .find()
      .then(results => {
         //if there are no results dont show the conatiner of the table
         if (results.items && results.items.length > 0) {
            //$w('#box17').expand()
         } else {
            //$w('#box17').collapse()
         }

         //loop through the results and create table rows with the data
         for (var i = 0; i < results.items.length; i++) {
            let data = {
               practitionerCategory: `<a href='https://lbdesignsofficial.wixsite.com/mysite/account-1/blank-3?testParam=${results.items[i].healthActivityJournalId}'>${results.items[i].practitionerCategory}</a>`,
               practitionerType: results.items[i].practitionerType,
               activityType: results.items[i].activityType,
               healthIssueJournalId: `<a href='https://lbdesignsofficial.wixsite.com/mysite/account-1/blank-1?testParam=${results.items[i].healthIssueJournalReference}'>${results.items[i].healthIssueJournalReference.split('_')[1]}</a>`
            }
            tableActivity.push(data);
         }

         //set the table rows
         $w('#table2').rows = tableActivity;
      });

   //get data for mroning wellness chart
   let morningData = await getMorningChartData()

   //create custom bar chart from data
   let M_args = {
      element: '#customElement1',
      data: morningData.dataset,
      labels: morningData.labels,
      type: 'bar'
   }
   createBarLine(M_args)

   //get data for evening wellness chart
   let eveningData = await getEveningChartData()

   //create custom bar chart from data
   let E_args = {
      element: '#customElement5',
      data: eveningData.dataset,
      labels: eveningData.labels,
      type: 'line'
   }
   createBarLine(E_args)

   //get data for polar chart
   let polarData = await getPolarChartData('day')

   //create custom polar chart from data
   let args4 = {
      element: '#customElement4',
      data: polarData.datasets,
      labels: polarData.labels,
      range: range
   }
   createPolar(args4)

   /*
   //TODO: update how this type of chart is made
   let args2 = {
      element: '#customElement2',
      data: data.dataset,
      labels: data.labels,
      type: 'line',
      //tension: 0.3 //used to make the line curved
   }
   createBarLine(args2)

   //TODO: update how this chart is made
   //TODO: get correct data from dataset for this chart
   let args3 = {
      element: '#customElement3',
      dataset: 'test_lineChart',
      type: 'line'
   }
   MonthChart(args3)
   */
});

/**
 * fucntion that toggles the data range to be displayed in the chart
 * 
 * @param {Event} event - event object
 */
export async function toggleData(event) {
   let text

   //cycle thorugh data based on date range
   if (event.target.id == 'nextBtn') {
      if (range == 'day') {
         range = 'week'
         text = 'Last 7 days'
      } else if (range == 'week') {
         range = 'month'
         text = 'Last 30 days'
      } else {
         range = 'day'
         text = 'Today'
      }
   } else {
      if (range == 'day') {
         range = 'month'
         text = 'Last 30 days'
      } else if (range == 'month') {
         range = 'week'
         text = 'Last 7 days'
      } else {
         range = 'day'
         text = 'Today'
      }
   }
   
   //get data for polar chart
   let polarData = await getPolarChartData(range)
   
   //create custom polar chart from data
   let args4 = {
      element: '#customElement4',
      data: polarData.datasets,
      labels: polarData.labels,
   }
   createPolar(args4)

   //update text display
   $w('#text209').text = text
}

/**
 * function to toggle between to different ranges of data
 */
export async function toggleBar() {
   let text
   let data

   //get data for specific modd/energy/health chart based on range
   if (barRange == 'week') {
      barRange = '7 days'
      text = 'Last 7 days'
      data = await getMorningChartData(true)
   } else {
      barRange = 'week'
      text = 'This Week'  
      data = await getMorningChartData()    
   }
   
   //create custom bar chart from data
   let args = {
      element: '#customElement1',
      data: data.dataset,
      labels: data.labels,
      type: 'bar'
   }
   createBarLine(args)
   
   //update text display
   $w('#text210').text = text
}