import { createPolar } from 'public/functions.js'
import { getPolarChartData, combinedPolar } from "public/charts.js";

let range = 'day'

$w.onReady(async function () {
	let polarData = await getPolarChartData('day')
	console.log(polarData)
	let test = await combinedPolar()
	console.log(test)

   //create custom polar chart from data
   	let args4 = {
      element: '#customElement1',
      data: test.datasets,
      labels: test.labels,
      range: range
   	}
   	createPolar(args4)
});