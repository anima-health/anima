import wixData from 'wix-data';
import wixUsers from 'wix-users';
import wixLocation from 'wix-location';
import { getDate } from 'public/functions.js';
import { healthAssessment, fitnessAssessment, dietAssessment } from 'public/assessments.js'

let taskCount = 0
let state = 'health'
$w.onReady(async function () {
   $w('#healthBtn').onClick(() => {
		$w('#assessmentStateBox').changeState("Health");
      state = 'health'
	});

	$w('#fitnessBtn').onClick(() => {
		$w('#assessmentStateBox').changeState("Fitness");
      state = 'fitness'
	});

   $w('#dietBtn').onClick(() => {
		$w('#assessmentStateBox').changeState("Diet");
      state = 'diet'
	});

   let dailyTasks = [
      {
         dataset: 'MorningHealthCheckResponseDB',
         btn: '#morningBtn',
         check: '#morningCheck'
      }, {
         dataset: 'EveningHealthCheckResponseDB',
         btn: '#eveningBtn',
         check: '#eveningCheck'
      }, {
         dataset: 'LifestyleHealthCheckResponseDB',
         btn: '#lifestyleBtn',
         check: '#lifestyleCheck'
      }
   ]

   await setDailyTasks(dailyTasks)

   await healthAssessment()
   await fitnessAssessment()
   await dietAssessment()
})

export async function setDailyTasks(items) {
   for (let i = 0; i < items.length; i++) {
      const element = items[i];

      await wixData.query(`${element.dataset}`)
      .contains('_owner', wixUsers.currentUser.id)
      .contains('date', getDate())
      .limit(1)
      .find()
      .then(res => {
         if (res.items.length == 0) {
            $w(`${element.btn}`).show()
            $w(`${element.check}`).hide()
            return false
         } else {
            $w(`${element.btn}`).hide()
            $w(`${element.check}`).show()
            taskCount++
            return true
         }
      })
   }

   if (taskCount == 3) {
      $w('#dailyProgressContainer').hide()
      $w('#allCompleteIcon').show()
      $w('#allCompletetxt').show()
      $w('#allCompleteBtn').show()
   }

   $w('#dailyProgressCount').text = taskCount.toString()
}

/**
*	send user to the health assessment page to update their data
*	 @param {$w.MouseEvent} event
*/
export function editProfile(event) {
   let link = ''
   switch (state) {
      case 'health':
         link = '/health-assessment?edit=true'
         break;
      case 'fitness':
         link = '/fitness-assessment?edit=true'
         break;
      case 'diet':
         link = '/diet-assessment?edit=true'
         break;
   }
	wixLocation.to(`${link}`);
}