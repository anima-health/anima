import wixUsers from 'wix-users';
import wixData from 'wix-data';
import wixLocation from 'wix-location';
import { insertData, setDropdown, getDropDownItems, dropDownFilter, dropDownDoubleFilter, getTags } from 'public/functions.js'

//array of custom element tag results
let tagResults = []

$w.onReady(async function() { 
   let query = wixLocation.query;
   //testParam
	$w('#healthActivityJournalRefDropdown').value = query.ref

   /**
    * list of args to create custom elements
    * @param {String} name - Identifier for the custom element
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    */
   let tagList = [{
      name: 'feeling',
      element: '#healthActivityJournalFeelingTagsCustomElement',
      dataset: 'Feelings',
      field: 'title'
   }]

   //loop through taglist array and create custom elements
   for (var i = 0; i < tagList.length; i++) {
      getTags(tagList[i])

      $w(`${tagList[i].element}`).on('my-event', (e) => {
         tagResults = e.detail.tags;
      });
   }

   /**
    * list of args for dropdown list 
    * @param {String} elemenst - the dropdown element
    * @param {String} dataset - dataset to get the options from
    * @param {String} key - the field in the dataset to use
    */
   let args = {
      element: '#healthActivityJournalPractitionerCatDropdown',
      dataset: 'PractitionerActivityMappingDB',
      key: 'practitionerCategory'
   }

   //sets dropdown options
   getDropDownItems(args)

   /**
    * list of args for dropdown list 
    * @param {String} elemenst - the dropdown element
    * @param {String} dataset - dataset to get the options from
    * @param {String} key - the field in the dataset to use
    */
   let journalArgs = {
      element: '#healthActivityJournalRefDropdown',
      dataset: 'HealthIssueJournalResponseDB',
      key: 'healthIssueJournalReference'
   }

   //sets dropdown options
   getDropDownItems(journalArgs, wixUsers.currentUser)
});

//change the health practitioner type dropdown based on the health practitioner category
export function categoryChange() {
   let args = {
      element: '#healthActivityJournalPractitionerTypeDropdown',
      dataset: 'PractitionerActivityMappingDB',
      key: 'practitionerType',
      filter_element: '#healthActivityJournalPractitionerCatDropdown',
      filter_key: 'practitionerCategory'
   }

   //sets dropdown options
   dropDownFilter(args)
}

//change the health activity type dropdown based on the health practitioner category & type
export function typeChange() {
   let args = {
      element: '#healthActivityJournalActivityTypeDropdown',
      dataset: 'PractitionerActivityMappingDB',
      key: 'activityType',
      filter_element: '#healthActivityJournalPractitionerCatDropdown',
      filter_key: 'practitionerCategory',
      filter_element_2: '#healthActivityJournalPractitionerTypeDropdown',
      filter_key_2: 'practitionerType'
   }

   //sets dropdown options
   dropDownDoubleFilter(args)
}

//function to submit data into the dataset
export function submitJournal() {
   let journalId

   //create a unquie journalID for the data being entered based on the dropdown box values
   if ($w('#healthActivityJournalActivityTypeDropdown').value === '' || $w('#healthActivityJournalActivityTypeDropdown').value === "I Don't Know") {
      if ($w('#healthActivityJournalPractitionerTypeDropdown').value === '' || $w('#healthActivityJournalPractitionerTypeDropdown').value === "I Don't Know") {
         journalId = $w('#healthActivityJournalRefDropdown').value + '_' + $w('#healthActivityJournalPractitionerCatDropdown').value
      } else {
         journalId = $w('#healthActivityJournalRefDropdown').value + '_' + $w('#healthActivityJournalPractitionerTypeDropdown').value
      }
   } else {
      journalId = $w('#healthActivityJournalRefDropdown').value + '_' + $w('#healthActivityJournalActivityTypeDropdown').value
   }

   //replace any spaces with underscores
   journalId = journalId.split(' ').join('_');


   //Set results Object to be submiited based on user inputed data
   let results = {
      userId: wixUsers.currentUser.id,
      healthActivityJournalId: journalId,
      healthIssueJournalReference: $w('#healthActivityJournalRefDropdown').value,
      practitionerCategory: $w('#healthActivityJournalPractitionerCatDropdown').value,
      practitionerType: $w('#healthActivityJournalPractitionerTypeDropdown').value,
      activityType: $w('#healthActivityJournalActivityTypeDropdown').value,
      satisfaction: $w('#healthActivityJournalSatisfactionSlider').value,
      relationship: $w('#healthActivityJournalRelationshipSlider').value,
      knowledge: $w('#healthActivityJournalEnvironmentSlider').value,
      environment: $w('#healthActivityJournalKnowledgeSlider').value,
      value: $w('#healthActivityJournalValueSlider').value,
      energy: $w('#healthActivityJournalEnergySlider').value,
      comfort: $w('#healthActivityJournalComfortSlider').value,
      mood: $w('#healthActivityJournalMoodSlider').value,
      feeling: tagResults,
      notes: $w('#healthActivityJournalNotesTextBox').value
   }

   //console.log(results)
   //Insert data into databse
   insertData('HealthActivityJournalResponseDB', results)
   wixLocation.to(`/insights`);   
}