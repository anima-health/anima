/**
 * naming
 * 
 * <MEAL TYPE>Fold<NUMBBER>         e.g. breakfastFold1
 * <MEAL TYPE>ArrowDown<NUMBBER>    e.g. breakfastArrowDown1
 * <MEAL TYPE>ArrowRight<NUMBBER>   e.g. breakfastArrowRight1  
 */

/**
 * function to collapse and open folds in accordion section
 * 
 * @param {Number} index - unique number of the fold
 * @param {String} meal - meal name
 */
function toggleFold(index, meal) {
	let $fold = $w(`#${meal}Fold${index}`);
	let $arrowDown = $w(`#${meal}ArrowDown${index}`);
	let $arrowRight = $w(`#${meal}ArrowRight${index}`);

   // toggle the fold at the index
	if ($fold.collapsed) {
		$fold.expand();
		$arrowDown.show();
		$arrowRight.hide();
	} else {
		$fold.collapse();
		$arrowDown.hide();
		$arrowRight.show();
	}

	// collapse the other folds
   for (let i = 1; i < 10; i++) {
      if (i != index){
         $w(`#${meal}Fold${i}`).collapse();
         $w(`#${meal}ArrowDown${i}`).hide();
         $w(`#${meal}ArrowRight${i}`).show();
      }
   }
}

/**
 * function to open the first section
 * 
 * @param {Event} event - event object that triggered this function
 */
export function openSection(event) {
	const id = event.target.id
	const fold = id.substr(id.length - 1)
   const meal = getMeal(id)

	toggleFold(parseInt(fold), meal)
}

/**
 * function to open section if the user selects the yes option
 * 
 * @param {Event} event - event object that triggered this function
 */
export function toggleOptions(event) {
	const id = event.target.id
   const meal = getMeal(id)

	if(event.target.value == 'Yes') {
		$w(`#${meal}Options`).expand()
	} else {
		$w(`#${meal}Options`).collapse()
	}
}

/**
 * function to retrieve meal name from element ID
 * 
 * @param {String} id - element ID
 * @returns meal name as string
 */
function getMeal(id) {
   if(id.includes("breakfast")) {
      return 'breakfast'
   } else if (id.includes("lunch")) {
      return 'lunch'
   } else if (id.includes("dinner")) {
      return 'dinner'
   } else {
      return ''
   }
}

/**
 * functin to submit data into dataset
 */
export function submitForm() {
	const breakfast = $w('#breakfastRadio').value
	let breakfastBread, breakfastFriut

   //if the user has selected breakfast set the value for the different breakfast options else leave them blank
	if (breakfast == 'Yes') {
		breakfastBread = $w('#selectionTags1').value
		breakfastFriut = $w('#selectionTags2').value
      //...more options here
	}

	const results = {
		breakfast: breakfast,
		breakfastBread: breakfastBread,
		breakfastFriut: breakfastFriut
      //...more options here
	}

	console.log(results)
}