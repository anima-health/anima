import wixUsers from 'wix-users';
import wixLocation from 'wix-location';
import { getCheckSliders, insertData, addIDontKnow, getDataFilter, getDate, dropDownFilter, dropDownDoubleFilter } from 'public/functions.js'

//arrays for dataset entry
let checkedSym = [];
let symSeverity = [];
let checkedPain = [];
let painSeverity = [];
let el = [];

$w.onReady(async function() {
   
   $w('#healthIssueJournalIssueStatusDropdown').value = wixLocation.query.conditon

   /**
    * list of args to create custom elements
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    * @param {String} name - Identifier for the custom element
    */
   let args = [{
      element: "#healthIssueJournalIssueSymptomsCheckboxCustomElement",
      dataset: "HealthIssueSymptomsMappingDB",
      field: "symptom",
      name: 'symptom',
      data: ["I Don't Know"]
   }, {
      element: "#healthIssueJournalPainSeveritySliderCustomElement",
      dataset: "HealthIssueJournalOptionsDB",
      field: "painType",
      name: 'pain',
      data: ["I Don't Know"]
   }]

   //Loop through the args array and create the custom elements
   for (let i = 0; i < args.length; i++) {
      //get the API from the custom element so the data can be changed later
      el[i] = await getCheckSliders(args[i])
   }

   //get the results from the custom element
   $w('#healthIssueJournalIssueSymptomsCheckboxCustomElement').on('my-event', (e) => {
      checkedSym = e.detail.checkboxes;
      symSeverity = e.detail.sliders;

      //only show pain sliders if pain is selected as a symptom
      let hasPain = false

      for (let i = 0; i < checkedSym.length; i++) {
         if (checkedSym[i] == 'Pain') {
            hasPain = true
         }         
      }

      if( hasPain == true ) {
         $w('#healthIssueJournalPainContainer').expand()
      } else {
         $w('#healthIssueJournalPainContainer').collapse()
      }
   });

   //get the results from the custom element
   $w('#healthIssueJournalPainSeveritySliderCustomElement').on('my-event', (e) => {
      checkedPain = e.detail.checkboxes;
      painSeverity = e.detail.sliders;
   });
});

//change the health issue area dropdown based on the health issue catergory
export function areaChange() {
   let args = {
      element: "#healthIssueJournalIssueAreaDropdown",
      dataset: "HealthIssueCategoryMappingDB",
      key: "healthIssueAreaCategory",
      filter_element: "#healthIssueJournalIssueCatDropdown",
      filter_key: "healthIssueCategory"
   }

   //sets dropdown options & enables the dropdown
   dropDownFilter(args);
   $w(args.element).enable();

   // get the options for the next dropdown if the value of the prevoius dropdown is not empty
   if ($w("#healthIssueJournalIssueNameDropdown").value !== '') {
      issueChange()
   }
}

//change the health issue name dropdown based on the health issue category & area
export function issueChange() {
   let args = {
      element: "#healthIssueJournalIssueNameDropdown",
      dataset: "HealthIssueCategoryMappingDB",
      key: "healthIssueName",
      filter_element: "#healthIssueJournalIssueCatDropdown",
      filter_key: "healthIssueCategory",
      filter_element_2: "#healthIssueJournalIssueAreaDropdown",
      filter_key_2: "healthIssueAreaCategory"
   }

   //sets dropdown options & enables the dropdown
   dropDownDoubleFilter(args);
   $w(args.element).enable();
}

/**
 * change the data for the symptom checkboxSlider custom element based on the dropdown box values
 * 
 * @param {Event} event - event handler for the function
 */
export async function symptomChange(event) {
   //show symptoms sliders
   $w('#healthIssueJournalIssueSymptomsContainer').expand()
   let customData = await getDataFilter('HealthIssueSymptomsMappingDB', 'symptom', 'issueName', event.target.value)

   //add additional options to the data for the custom element
   customData = addIDontKnow(customData)

   //set custom data to the custom element
   el[0].data = customData
}

//function to submit data into the dataset
export async function SubmitData() {

   //get unique journal ID for the data that is being submitted
   let journalID = await getJournalID();

   //Set results Object to be submiited based on user inputed data
   let insertResults = {
      status:                       $w('#healthIssueJournalIssueStatusDropdown').value,
      startDate:                    $w('#healthIssueJournalIssueStartDatePicker').value,
      currentSymptoms:              $w('#healthIssueJournalCurrentSymptomsRadioButton').value,
      healthIssueCategory:          $w('#healthIssueJournalIssueCatDropdown').value,
      healthIssueAreaCategory:      $w('#healthIssueJournalIssueAreaDropdown').value,
      healthIssueName:              $w('#healthIssueJournalIssueNameDropdown').value,
      symptoms:                     checkedSym,
      symptomSeverity:              symSeverity,
      painType:                     checkedPain,
      painSeverity:                 painSeverity,
      healthActivity:               $w('#healthIssueJournalActivityDropdown').value,
      //formId:                       journalID,
      userId:                       wixUsers.currentUser.id,
      healthIssueJournalReference:  journalID
   }

   //console.log(insertResults)
   //Insert data into databse
   insertData('HealthIssueJournalResponseDB', insertResults)
   if(insertResults.healthActivity == 'Yes') {
      //after creating journal send user to the health activity page for this journal
      wixLocation.to(`/health-activity-journal?ref=${journalID}`);
   } else {
      wixLocation.to(`/insights`);
   }
}

/**
 * show current symptoms radio buttons
 */
export function statusChange(event) {
	const val = event.target.value
   
   if(val == 'Ongoing' || val == 'Hereditary') {
      $w('#healthIssueJournalCurrentSymptomsGroup').expand()
   } else {
      $w('#healthIssueJournalCurrentSymptomsGroup').collapse()
   }
}

/**
 * function to create unique journal id for the form
 * 
 * @returns journal id to be inserted into dataset
 */
async function getJournalID() {
   let journalID

   //If there is a start date for the issue set in the format dd/mm/yyyy
   let startDate = null

   //set startDate to correct format
   startDate = $w('#healthIssueJournalIssueStartDatePicker').value
   startDate = getDate(startDate)

   //set date to current if startDate is not set
   let date = startDate ? startDate : await getDate();

   //create a unquie journalID for the data being entered based on the dropdown box values
   if ($w('#healthIssueJournalIssueNameDropdown').value === '' || $w('#healthIssueJournalIssueNameDropdown').value === "I Don't Know") {
      if ($w('#healthIssueJournalIssueAreaDropdown').value === '' || $w('#healthIssueJournalIssueAreaDropdown').value === "I Don't Know") {
         journalID = date + '_' + $w('#healthIssueJournalIssueCatDropdown').value
      } else {
         journalID = date + '_' + $w('#healthIssueJournalIssueAreaDropdown').value
      }
   } else {
      journalID = date + '_' + $w('#healthIssueJournalIssueNameDropdown').value
   }
   journalID = journalID.split(' ').join('_');

   return journalID
}  