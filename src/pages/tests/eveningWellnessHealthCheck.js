import wixLocation from 'wix-location';
import wixUsers from 'wix-users';
import { getTags, getDate, insertDataDaily, getData } from 'public/functions.js'

let feelingTags = []

$w.onReady(async function() {
   /**
    * list of args to create custom elements
    * @param {String} name - Identifier for the custom element
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    */
   let tagList = [{
      name: 'mood',
      element: '#eveningWellnessFeelingTagsCustomElement',
      dataset: 'Feelings',
      field: 'title'
   }]

   //Loop through the tag Object and create the tag custom elements
   for (var i = 0; i < tagList.length; i++) {
      getTags(tagList[i])

      //set tag results to be inserted into dataset later
      $w(`${tagList[i].element}`).on('my-event', (e) => {
         feelingTags = e.detail.tags;
      });
   }
});

//Submit fitness assessment data into dataset
export async function submitData() {
   //Set results Object to be submiited based on user inputed data
	let results = {
		'uid': wixUsers.currentUser.id,
      'date': getDate(),
		'health': $w('#eveningWellnessHealthSlider').value,
		'energy': $w('#eveningWellnessEnergySlider').value,
		'diet': $w('#eveningWellnessDietSlider').value,
		'fitness': $w('#eveningWellnessFitnessSlider').value,
		'comfort': $w('#eveningWellnessComfortSlider').value,
		'mood': $w('#eveningWellnessMoodSlider').value,
		'calmness': $w('#eveningWellnessCalmnessSlider').value,
		'social': $w('#eveningWellnessSocialSlider').value,
		'leisure': $w('#eveningWellnessLeisureSlider').value,
      'work': $w('#eveningWellnessWorkSlider').value,
		'money': $w('#eveningWellnessMoneySlider').value,
		'lifeAdmin': $w('#eveningWellnessLifeTasksSlider').value,
		'feeling': feelingTags
	}
	
   //Insert data into databse, only allowed once a day per user
	await insertDataDaily('EveningHealthCheckResponseDB', results)
   wixLocation.to(`/insights`);
}