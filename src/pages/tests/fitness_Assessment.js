import { getDropDownItems, getTags, getCheckboxes, getDate, insertDataDaily } from 'public/functions.js'
import wixLocation from 'wix-location';
import wixData from 'wix-data';
import wixUsers from 'wix-users';

//list of arrays for dataset entry
let exerciseTypes = [];
let locationStyle = [];
let motivationStyle = [];
let motivators = [];
let demotivators = [];
let cardio = [];
let combination = [];
let functional = [];
let racket = [];
let strength = [];
let team = [];

$w.onReady(async function() {
   /**
    * list of args to create checkbox custom elements
    * @param {String} name - Identifier for the custom element
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    */
   let checkboxList = [{
      name: 'exercise',
      element: '#fitnessAssessmentExerciseTypesCheckboxCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'exerciseTypes'
   }, {
      name: 'location',
      element: '#fitnessAssessmentLocationStyleCheckboxCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'locationStyle'
   }, {
      name: 'motivation',
      element: '#fitnessAssessmentMotivationStyleCheckboxCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'motivationStyle'
   }, {
      name: 'motivators',
      element: '#fitnessAssessmentMotivatorsCheckboxCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'motivators'
   }, {
      name: 'demotivators',
      element: '#fitnessAssessmentDemotivatorsCheckboxCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'demotivators'
   }]

   /**
    * list of args to create tags custom elements
    * @param {String} name - Identifier for the custom element
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    */
   let tagList = [{
      name: 'cardio',
      element: '#fitnessAssessmentCardioTagsCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'cardio'
   }, {
      name: 'combination',
      element: '#fitnessAssessmentComboTagsCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'combination'
   }, {
      name: 'functional',
      element: '#fitnessAssessmentFunctionalTagsCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'functional'
   }, {
      name: 'racket',
      element: '#fitnessAssessmentRacketTagsCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'racket'
   }, {
      name: 'strength',
      element: '#fitnessAssessmentStrengthTagsCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'strength'
   }, {
      name: 'team',
      element: '#fitnessAssessmentTeamTagsCustomElement',
      dataset: 'FitnessAssessmentOptionsDB',
      field: 'team'
   }]

   /**
    * list of args to create dropdown options
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} key - Field name in the dataset to be used as options
    */
   let dropDownArgs = [{
      element: "#fitnessAssessmentActivityLevelDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "activityLevel"
   }, {
      element: "#fitnessAssessmentExerciseFreqDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "exerciseFrequency"
   }, {
      element: "#fitnessAssessmentExerciseDurationDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "weeklyExerciseDuration"
   }, {
      element: "#fitnessAssessmentFreqStyleDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "frequencyStyle"
   }, {
      element: "#fitnessAssessmentRiskBreathingDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "breathingIssues"
   }, {
      element: "#fitnessAssessmentRiskChestDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "chestPain"
   }, {
      element: "#fitnessAssessmentRiskDehydrationDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "dehydration"
   }, {
      element: "#fitnessAssessmentRiskDizzinessDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "dizzyness"
   }, {
      element: "#fitnessAssessmentRiskNauseaDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "nauseaVomiting"
   }, {
      element: "#fitnessAssessmentRiskPainDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "pain"
   }, {
      element: "#fitnessAssessmentFactorsWarmUpDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "warmUp"
   }, {
      element: "#fitnessAssessmentFactorsCoolDownDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "coolDown"
   }, {
      element: "#fitnessAssessmentFactorsClothingDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "appropriateClothing"
   }, {
      element: "#fitnessAssessmentFactorsFormDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "correctForm"
   }, {
      element: "#fitnessAssessmentFactorsHydrationDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "hydration"
   }, {
      element: "#fitnessAssessmentFactorsNourishmentDropdown",
      dataset: "FitnessAssessmentOptionsDB",
      key: "nourishment"
   }]

   //Loop through the checkbox Object and create the checkbox custom elements
   for (var i = 0; i < checkboxList.length; i++) {
      getCheckboxes(checkboxList[i])
   }

   //Loop through the tag Object and create the tag custom elements
   for (var i = 0; i < tagList.length; i++) {
      getTags(tagList[i])
   }

   //Loop through the dropdown Object and set the correct options for each one
   for (var i = 0; i < dropDownArgs.length; i++) {
      getDropDownItems(dropDownArgs[i]);
   }

   /**
    * Event listeners for all the custom elements on the page
    */

   $w('#fitnessAssessmentExerciseTypesCheckboxCustomElement').on('my-event', (e) => {
      //Set exerciseTypes to array of checked checkboxes 
      exerciseTypes = e.detail.checkboxes;

      //If exerciseTypes array is not empty show 'FitnessAssessment' area
      if (exerciseTypes.length > 0) {
         $w('#FitnessAssessmentActivitiesContainer').expand()
         $w('#fitnessAssessmentActivitiesHeader').expand()
      } else {
         $w('#FitnessAssessmentActivitiesContainer').collapse()
         $w('#fitnessAssessmentActivitiesHeader').collapse()
      }

      //Show or Hide 'exerciseTypes' category area based on whether it has been checked
      //Cardio
      if (exerciseTypes.includes('Cardio') === true) {
         $w('#fitnessAssessmentCardioContainer').expand()
      } else if (exerciseTypes.includes('Cardio') === false) {
         $w('#fitnessAssessmentCardioContainer').collapse()
      }
      //Combination
      if (exerciseTypes.includes('Combination') === true) {
         $w('#fitnessAssessmentComboContainer').expand()
      } else if (exerciseTypes.includes('Combination') === false) {
         $w('#fitnessAssessmentComboContainer').collapse()
      }
      //Functional
      if (exerciseTypes.includes('Functional') === true) {
         $w('#fitnessAssessmentFunctionalContainer').expand()
      } else if (exerciseTypes.includes('Functional') === false) {
         $w('#fitnessAssessmentFunctionalContainer').collapse()
      }
      //Racket
      if (exerciseTypes.includes('Racket') === true) {
         $w('#fitnessAssessmentRacketContainer').expand()
      } else if (exerciseTypes.includes('Racket') === false) {
         $w('#fitnessAssessmentRacketContainer').collapse()
      }
      //Strength
      if (exerciseTypes.includes('Strength') === true) {
         $w('#fitnessAssessmentStrengthContainer').expand()
      } else if (exerciseTypes.includes('Strength') === false) {
         $w('#fitnessAssessmentStrengthContainer').collapse()
      }
      //Team
      if (exerciseTypes.includes('Team') === true) {
         $w('#fitnessAssessmentTeamContainer').expand()
      } else if (exerciseTypes.includes('Team') === false) {
         $w('#fitnessAssessmentTeamContainer').collapse()
      }
   })

   /**
    * event listeners for the custom elements to get the values to be inserted into dataset
    */
   $w('#fitnessAssessmentLocationStyleCheckboxCustomElement').on('my-event', (e) => {
      locationStyle = e.detail.checkboxes;
   })

   $w('#fitnessAssessmentLocationStyleCheckboxCustomElement').on('my-event', (e) => {
      motivationStyle = e.detail.checkboxes;
   })

   $w('#fitnessAssessmentMotivatorsCheckboxCustomElement').on('my-event', (e) => {
      motivators = e.detail.checkboxes;
   })

   $w('#fitnessAssessmentDemotivatorsCheckboxCustomElement').on('my-event', (e) => {
      demotivators = e.detail.checkboxes;
   })

   $w('#fitnessAssessmentCardioTagsCustomElement').on('my-event', (e) => {
      cardio = e.detail.tags;
   })

   $w('#fitnessAssessmentTeamTagsCustomElement').on('my-event', (e) => {
      team = e.detail.tags;
   })

   $w('#fitnessAssessmentComboTagsCustomElement').on('my-event', (e) => {
      combination = e.detail.tags;
   })

   $w('#fitnessAssessmentFunctionalTagsCustomElement').on('my-event', (e) => {
      functional = e.detail.tags;
   })

   $w('#fitnessAssessmentRacketTagsCustomElement').on('my-event', (e) => {
      racket = e.detail.tags;
   })

   $w('#fitnessAssessmentStrengthTagsCustomElement').on('my-event', (e) => {
      strength = e.detail.tags;
   })
});

//submit fitness assessment data into dataset
export function submitFitness() {
   //Set results Object to be submiited based on user inputed data
   let results = {
      'uid': wixUsers.currentUser.id,
      'date': getDate(),
      'activityLevel': $w('#fitnessAssessmentActivityLevelDropdown').value,
      'exerciseTypes': exerciseTypes,
      'exerciseFrequency': $w('#fitnessAssessmentExerciseFreqDropdown').value,
      'weeklyExerciseDuration': $w('#fitnessAssessmentExerciseDurationDropdown').value,
      'locationStyle': locationStyle,
      'frequencyStyle': $w('#fitnessAssessmentFreqStyleDropdown').value,
      'motivationStyle': motivationStyle,
      'motivators': motivators,
      'demotivators': demotivators,
      'cardio': cardio,
      'combination': combination,
      'functional': functional,
      'racket': racket,
      'strength': strength,
      'team': team,
      'breathingIssues': $w('#fitnessAssessmentRiskBreathingDropdown').value,
      'chestPain': $w('#fitnessAssessmentRiskChestDropdown').value,
      'dehydration': $w('#fitnessAssessmentRiskDehydrationDropdown').value,
      'dizzyness': $w('#fitnessAssessmentRiskDizzinessDropdown').value,
      'nauseaVomiting': $w('#fitnessAssessmentRiskNauseaDropdown').value,
      'pain': $w('#fitnessAssessmentRiskPainDropdown').value,
      'warmUp': $w('#fitnessAssessmentFactorsWarmUpDropdown').value,
      'coolDown': $w('#fitnessAssessmentFactorsCoolDownDropdown').value,
      'appropriateClothing': $w('#fitnessAssessmentFactorsClothingDropdown').value,
      'correctForm': $w('#fitnessAssessmentFactorsFormDropdown').value,
      'hydration': $w('#fitnessAssessmentFactorsHydrationDropdown').value,
      'nourishment': $w('#fitnessAssessmentFactorsNourishmentDropdown').value
   }

   insertDataDaily('FitnessAssessmentResponseDB', results)
   wixLocation.to(`/insights`);
}