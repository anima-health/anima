import wixLocation from 'wix-location';
import wixData from 'wix-data';
import wixUsers from 'wix-users';
import { getCheckboxes, updateData, insertData, exapandCollapse, getData } from 'public/functions.js'

//arrays for data to be inserted into dataset
let highRisk = []
let drugType = []
let medType = []
let checkEls = []
let updateID = ''

$w.onReady(async function() {
   /**
    * list of args to create dropdown options
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    * @param {String} name - Name to be used as the class identifier
    */
	let args = [{
      element: '#healthAssessmentHighRiskCondCustomElement',
      dataset: 'HealthAssessmentOptionsDB',
      field: 'highRiskConditions',
      name: 'HighRisk'
   }, {
      element: '#healthAssessmentDrugTypesCustomElement',
      dataset: 'HealthAssessmentOptionsDB',
      field: 'recreationalDrugTypes',
      name: 'drugType'
   }, {
      element: '#healthAssessmentMedTypesCustomElement',
      dataset: 'HealthAssessmentOptionsDB',
      field: 'medicationTypes',
      name: 'medType'
   }]

   //loop through the custom element arguments array to create checkbox custom elements
   for (let i = 0; i < args.length; i++) {
      checkEls[i] = await getCheckboxes(args[i])
   }

   if(wixLocation.query.edit == 'true') {
      getDetails()
   }

   /**
    * event listeners for the custom elements to get the values to be inserted into dataset
    */
   $w('#healthAssessmentHighRiskCondCustomElement').on('my-event', (e) => {
      highRisk = e.detail.checkboxes;
   });

   $w('#healthAssessmentDrugTypesCustomElement').on('my-event', (e) => {
      drugType = e.detail.checkboxes;
   });

   $w('#healthAssessmentMedTypesCustomElement').on('my-event', (e) => {
      medType = e.detail.checkboxes;
   });
});

async function getDetails() {
   let data = await wixData.query(`HealthAssessmentResponseDB`)
      .contains('_owner', wixUsers.currentUser.id)
      .limit(1)
      .find()
      .then(res => {
         return res.items[0]
      });
   
   updateID = data._id

   $w('#healthAssessmentHighRiskCondCustomElement').value = data.highRiskConditions
   $w('#healthAssessmentHeightNumberInput').value = data.height.toString()
   $w('#healthAssessmentHeightUnitDropdown').value = data.heightUnit
   $w('#healthAssessmentWeightNumberInput').value = data.weight.toString()
   $w('#healthAssessmentWeightUnitDropdown').value = data.weightUnit
   $w('#healthAssessmentWaistNumberInput').value = data.waist.toString()
   $w('#healthAssessmentWaistUnitDropdown').value = data.waistUnit
   $w('#healthAssessmentHipsNumberInput').value = data.hips.toString()
   $w('#healthAssessmentHipsUnitDropdown').value = data.hipsUnit
   $w('#healthAssessmentBloodTypeDropdown').value = data.bloodType
   $w('#healthAssessmentHeartRateNumberInput').value = data.heartRateBpm.toString()
   $w('#healthAssessmentPreExistingCondDropdown').value = data.preExistingConditions
   $w('#healthAssessmentHereditaryCondDropdown').value = data.hereditaryConditions
   $w('#healthAssessmentAlcoholDropdown').value = data.alcohol
   $w('#healthAssessmentAlcoholUnitsDropdown').value = data.alcoholUnits
   $w('#healthAssessmentTobaccoDropdown').value = data.tobacco
   $w('#healthAssessmentTobaccoUnitsDropdown').value = data.tobaccoUnits
   $w('#healthAssessmentDrugsDropdown').value = data.recreationalDrugs
   $w('#healthAssessmentDrugsFreqDropdown').value = data.recreationalDrugFrequency
   $w('#healthAssessmentMedsDropdown').value = data.medication
   $w('#healthAssessmentSleepDropdown').value = data.sleepHours
   $w('#healthAssessmentScreenTimeDropdown').value = data.screentime
   
   for (let i = 0; i < checkEls.length; i++) {
      const element = checkEls[i];
      if(element._name == 'HighRisk') {
         checkEls[i].checked = data.highRiskConditions
         highRisk = data.highRiskConditions
      } else if(element._name == 'drugType') {
         checkEls[i].checked = data.recreationalDrugTypes
         drugType = data.recreationalDrugTypes
      } else if(element._name == 'medType') {
         checkEls[i].checked = data.medicationTypes
         medType = data.medicationTypes
      }
   }

   let e = {
      target: $w('#healthAssessmentAlcoholDropdown')
   }
   showUnits(e)
   e.target = $w('#healthAssessmentTobaccoDropdown')
   showTobacco(e)
   e.target = $w('#healthAssessmentDrugsDropdown')
   showDrugs(e)
   e.target = $w('#healthAssessmentMedsDropdown')
   showMeds(e)
   e.target = $w('#healthAssessmentPreExistingCondDropdown')
   preExistingChange(e)
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function showUnits(event) {
   exapandCollapse(event, '#healthAssessmentAlcoholUnitsContainer')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function showTobacco(event) {
   exapandCollapse(event, '#healthAssessmentTobaccoUnitsGroup')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function showDrugs(event) {
   exapandCollapse(event, '#healthAssessmentDrugsFreqGroup', '#healthAssessmentDrugTypesGroup')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function showMeds(event) {
   exapandCollapse(event, '#healthAssessmentMedTypesGroup')
}

//Submit data into dataset
export function submitData() {
   //Set results Object to be submiited based on user inputed data
   let results = {
      height: parseInt($w('#healthAssessmentHeightNumberInput').value),
      heightUnit: $w('#healthAssessmentHeightUnitDropdown').value,
      weight: parseInt($w('#healthAssessmentWeightNumberInput').value),
      weightUnit: $w('#healthAssessmentWeightUnitDropdown').value,
      waist: parseInt($w('#healthAssessmentWaistNumberInput').value),
      waistUnit: $w('#healthAssessmentWaistUnitDropdown').value,
      hips: parseInt($w('#healthAssessmentHipsNumberInput').value),
      hipsUnit: $w('#healthAssessmentHipsUnitDropdown').value,
      bloodType: $w('#healthAssessmentBloodTypeDropdown').value,
      heartRateBpm: parseInt($w('#healthAssessmentHeartRateNumberInput').value),
      preExistingConditions: $w('#healthAssessmentPreExistingCondDropdown').value,
      hereditaryConditions: $w('#healthAssessmentHereditaryCondDropdown').value,
      highRiskConditions: highRisk,
      alcohol: $w('#healthAssessmentAlcoholDropdown').value,
      alcoholUnits: $w('#healthAssessmentAlcoholUnitsDropdown').value,
      tobacco: $w('#healthAssessmentTobaccoDropdown').value,
      tobaccoUnits: $w('#healthAssessmentTobaccoUnitsDropdown').value,
      recreationalDrugs: $w('#healthAssessmentDrugsDropdown').value,
      recreationalDrugFrequency: $w('#healthAssessmentDrugsFreqDropdown').value,
      recreationalDrugTypes: drugType,
      medication: $w('#healthAssessmentMedsDropdown').value,
      medicationTypes: medType,
      sleepHours: $w('#healthAssessmentSleepDropdown').value,
      screentime: $w('#healthAssessmentScreenTimeDropdown').value
   }

   let param
   if(results.hereditaryConditions == 'Yes') {
      param = '?conditon=Hereditary'
   } else if (results.preExistingConditions == 'Yes') {
      param = '?conditon=Ongoing'
   }

   if(updateID) {
      updateData('HealthAssessmentResponseDB', results, updateID)
      
      if(param) {
         wixLocation.to(`/health-issue-journal${param}`);
      } else {
         wixLocation.to(`/account-1/blank`);
      }
   } else {
      //Insert data into databset
      insertData('HealthAssessmentResponseDB', results)

      if(param) {
         wixLocation.to(`/health-issue-journal${param}`);
      }      
   }
}

export function preExistingChange(event) {
   if(event.target.value == 'Yes') {
      $w('#healthAssessmentHighRiskCheckboxContainer').expand()
   } else {
      $w('#healthAssessmentHighRiskCheckboxContainer').collapse()
   }
}