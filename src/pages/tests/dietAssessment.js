import wixLocation from 'wix-location';
import { getCheckboxes, insertData } from 'public/functions.js'

let dietType = []
let dietAllergies = []
let dietAversions = []

$w.onReady(function () {
	/**
    * list of args to create checkbox custom elements
    * @param {String} name - Identifier for the custom element
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    * @param {Array} data - Array of additional options to be added to custom element
    */
   let checkboxList = [{
      name: 'dietType',
      element: '#dietAssessmentTypeCustomElement',
      dataset: 'DietAssessmentOptionsDB',
      field: 'dietType',
	   data: ['Other', 'All Food Groups - No Restrictions']
   },{
      name: 'dietAllergies',
      element: '#dietAssessmentAllergiesCustomElement',
      dataset: 'DietAssessmentOptionsDB',
      field: 'dietAllergies',
	   data: ['Other']
   },{
      name: 'dietAversions',
      element: '#dietAssessmentAversionsCustomElement',
      dataset: 'DietAssessmentOptionsDB',
      field: 'dietAversions',
	   data: ['Other']
   }]

   //Loop through the checkbox Object and create the checkbox custom elements
   for (var i = 0; i < checkboxList.length; i++) {
      getCheckboxes(checkboxList[i])
   }

   //Set checkbox values to be submitted into dataset
   $w('#dietAssessmentTypeCustomElement').on('my-event', (e) => {
      dietType = e.detail.checkboxes;
   })

   //Set checkbox values to be submitted into dataset
   $w('#dietAssessmentAllergiesCustomElement').on('my-event', (e) => {
      dietAllergies = e.detail.checkboxes;
   })

   //Set checkbox values to be submitted into dataset
   $w('#dietAssessmentAversionsCustomElement').on('my-event', (e) => {
      dietAversions = e.detail.checkboxes;
   })
});

//submit data into dataset
export function submitData() {
   //Set results Object to be submiited based on user inputed data
	let results = {
		dietProfile: $w('#dietAssessmentProfileDropdown').value,
		dietType: dietType,
		dietAllergies: dietAllergies,
		dietAversions: dietAversions,
		calories: $w('#dietAssessmentCalorieDropdown').value,
		mealFrequency: $w('#dietAssessmentMealFreqDropdown').value,
		boughtMealFrequency: $w('#dietAssessmentBoughtMealFreqDropdown').value,
		cakesBiscuits: $w('#dietAssessmentCakeDropdown').value,
		friedFood: $w('#dietAssessmentFriedDropdown').value,
		processedMeats: $w('#dietAssessmentProcessedMeatDropdown').value,
		refinedCarbs: $w('#dietAssessmentRefinedCarbsDropdown').value,
		sugaryDrinks: $w('#dietAssessmentSugaryDrinksDropdown').value,
		sweetsChocolate: $w('#dietAssessmentSweetsDropdown').value,
		fruitVeg: $w('#dietAssessmentFruitVegDropdown').value,
		leanProtein: $w('#dietAssessmentProteinDropdown').value,
		wholeGrains: $w('#dietAssessmentGrainsDropdown').value,
		healthyFats: $w('#dietAssessmentHealthyFatsDropdown').value,
		beansPulses: $w('#dietAssessmentBeansPulsesDropdown').value,
		nutsSeeds: $w('#dietAssessmentNutsSeedsDropdown').value,
	}

	//console.log(results)
	insertData('DietAssessmentResponseDB', results)
   wixLocation.to(`/insights`);
}