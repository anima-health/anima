import wixLocation from 'wix-location';
import wixUsers from 'wix-users';
import { insertDataDaily, getDate, exapandCollapse, getRadio, getCheckboxes } from 'public/functions.js'

//Submit fitness assessment data into dataset
export async function submitData() {
   //Set results Object to be submiited based on user inputed data
   let results = {
      'uid': wixUsers.currentUser.id,
      'date': getDate(),
      alcoholUnits: $w('#lifestyleHealthCheckAlcoholRadioButton').value,
      tobaccoUnits: $w('#lifestyleHealthCheckTobaccoRadioButton').value,
      screenTime: $w('#lifestyleHealthCheckScreenTimeRadioButton').value,
      meditationTime: $w('#lifestyleHealthCheckMeditationRadioButton').value,
      breakfast: $w('#lifestyleHealthCheckBreakfastRadioButton').value,
      breakfastEnergy: $w('#lifestyleHealthCheckBreakfastEnergySlider').value,
      breakfastMood: $w('#lifestyleHealthCheckBreakfastMoodSlider').value,
      lunch: $w('#lifestyleHealthCheckLunchRadioButton').value,
      lunchEnergy: $w('#lifestyleHealthCheckLunchEnergySlider').value,
      lunchMood: $w('#lifestyleHealthCheckLunchMoodSlider').value,
      dinner: $w('#lifestyleHealthCheckDinnerRadioButton').value,
      dinnerEnergy: $w('#lifestyleHealthCheckDinnerEnergySlider').value,
      dinnerMood: $w('#lifestyleHealthCheckDinnerMoodSlider').value,
      physicalActivity: $w('#lifestyleHealthCheckPhysicalActivityCheckRadioButton').value,
      physicalActivityCategory: $w('#lifestyleHealthCheckPhysicalActivityCatCheckbox').value,
      physicalActivityEnergy: $w('#lifestyleHealthCheckPhysicalActivityEnergySlider').value,
      physicalActivityMood: $w('#lifestyleHealthCheckPhysicalActivityMoodSlider').value,
      leisureActivity: $w('#lifestyleHealthCheckLeisureActivityCheckRadioButton').value,
      leisureActivityCategory: $w('#lifestyleHealthCheckLeisureActivityCatCheckbox').value,
      leisureActivityEnergy: $w('#lifestyleHealthCheckLeisureActivityEnergySlider').value,
      leisureActivityMood: $w('#lifestyleHealthCheckLeisureActivityMoodSlider').value
   }

   //Insert data into databse, only allowed once a day per user
   await insertDataDaily('LifestyleHealthCheckResponseDB', results)
   wixLocation.to(`/insights`);
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function checkBreakfast(event) {
   exapandCollapse(event, '#lifestyleHealthCheckBreakfastSliderContainer')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function checkLunch(event) {
   exapandCollapse(event, '#lifestyleHealthCheckLunchSliderContainer')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function checkDinner(event) {
   exapandCollapse(event, '#lifestyleHealthCheckDinnerSliderContainer')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function checkPhysActivity(event) {
   exapandCollapse(event, '#lifestyleHealthCheckPhysicalActivityCatContainer', '#lifestylePhysicalActivitySliderContainer')
}

/**
 * funciton to expand or collapse specfic element
 * 
 * @param {Event} event - event object that triggers the event
 */
export function checkLeisureActivity(event) {
   exapandCollapse(event, '#lifestyleHealthCheckLeisureActivityCatContainer', '#lifestyleHealthCheckLeisureActivitySliderContainer')
}