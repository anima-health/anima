import wixUsers from 'wix-users';
import wixLocation from 'wix-location';
import { getTags, getDate, insertDataDaily } from 'public/functions.js'

let feelingTags = []

$w.onReady(async function() {
   /**
    * list of args to create custom elements
    * @param {String} name - Identifier for the custom element
    * @param {String} elemenst - Element ID for the custom element to be attached to
    * @param {String} dataset - Dataset to be used 
    * @param {String} field - Field name in the dataset to be used as options
    */
   let tagList = [{
      name: 'feeling',
      element: '#morningWellnessFeelingTagsCustomElement',
      dataset: 'Feelings',
      field: 'title'
   }]

   //Loop through the tag Object and create the tag custom elements
   for (var i = 0; i < tagList.length; i++) {
      getTags(tagList[i])

      //set tag results to be inserted into dataset later
      $w(`${tagList[i].element}`).on('my-event', (e) => {
         feelingTags = e.detail.tags;
      });
   }
});

//Submit fitness assessment data into dataset
export async function submitData() {
   //Set results Object to be submiited based on user inputed data
	let results = {
		'uid': wixUsers.currentUser.id,
      'date': getDate(),
		'sleep': $w('#morningWellnessSleepSlider').value,
		'health': $w('#morningWellnessHealthSlider').value,
		'energy': $w('#morningWellnessEnergySlider').value,
		'comfort': $w('#morningWellnessComfortSlider').value,
		'mood': $w('#morningWellnessMoodSlider').value,
		'feeling': feelingTags
	}
	
   //Insert data into databse, only allowed once a day per user
	await insertDataDaily('MorningHealthCheckResponseDB', results)
   wixLocation.to(`/insights?data=morning`);
}